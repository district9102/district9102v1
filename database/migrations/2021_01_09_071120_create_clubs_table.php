<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Club', function (Blueprint $table) {
            $table->id();
            //$table->integer('IdResponsable');
            $table->string('NomClub');
            $table->string('Historique');
            $table->string('ActionFM');
            $table->foreignId('PaysId')->constrained('Pays');
            $table->foreignId('ResponsableId')->constrained('Personne');
            $table->foreignId('CategorieId')->constrained('Categorie');
            $table->integer('Parrain1')->nullable();
            $table->integer('Parrain2')->nullable();
            $table->integer('Parrain3')->nullable();
            $table->timestamps();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Club');
    }
}
