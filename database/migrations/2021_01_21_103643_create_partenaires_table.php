<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartenairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Partenaire', function (Blueprint $table) {
            $table->id();
            $table->string('Nom');
            $table->string('Email');
            $table->string('Telephone');
            $table->string('Adresse');
            $table->string('Service');
            $table->integer('CodePartenaire');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Partenaire');
    }
}
