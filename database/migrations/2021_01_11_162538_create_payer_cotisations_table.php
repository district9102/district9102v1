<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayerCotisationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PayerCotisation', function (Blueprint $table) {
            $table->primary(['CotisationId', 'PersonneId']);
            $table->foreignId('CotisationId')->nullable()->constrained('Cotisation');
            $table->foreignId('PersonneId')->nullable()->constrained('Personne');
            $table->foreignId('AnneeRotarienneId')->nullable()->constrained('AnneeRotarienne');
            $table->date('Date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PayerCotisation');
    }
}
