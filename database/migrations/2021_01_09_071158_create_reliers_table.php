<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Relier', function (Blueprint $table) {
            $table->primary(['AnneeRotarienneId', 'ClubId', 'PerformanceId']);
            $table->foreignId('AnneeRotarienneId')->nullable()->constrained('AnneeRotarienne');
            $table->foreignId('ClubId')->nullable()->constrained('Club');
            $table->foreignId('PerformanceId')->nullable()->constrained('Performance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Relier');
    }
}
