<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEvenementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Evenement', function (Blueprint $table) {
            $table->id();
            $table->string('Titre')->nullable();
            $table->date('DatePublication')->nullable();
            $table->string('PieceJointe')->nullable();
            $table->date('Jour')->nullable();
            $table->string('HeureDebut')->nullable();
            $table->string('HeureFin')->nullable();
            $table->string('GrandPoint')->nullable();
            $table->string('SynthesePoint1')->nullable();
            $table->string('SynthesePoint2')->nullable();
            $table->string('SynthesePoint3')->nullable();
            $table->string('SynthesePoint4')->nullable();
            $table->string('SynthesePoint5')->nullable();
            $table->string('CommunicationStatutaire')->nullable();
            $table->string('Quorum')->nullable();
            $table->string('DescriptionCourte')->nullable();
            $table->string('AxeStrategique')->nullable();
            $table->string('RapportCommission1')->nullable();
            $table->string('RapportCommission2')->nullable();
            $table->string('RapportCommission3')->nullable();
            $table->string('RapportCommission4')->nullable();
            $table->string('RapportCommission5')->nullable();
            $table->string('RapportCommission6')->nullable();
            $table->string('RapportCommission7')->nullable();
            $table->string('RapportCommission8')->nullable();
            $table->string('RapportCommission9')->nullable();
            $table->string('RapportCommission10')->nullable();
            $table->string('LieuReunion')->nullable();
            $table->string('VilleEvenement')->nullable();
            $table->foreignId('ClubId')->constrained('Club');
            $table->foreignId('TypeEvenementId')->constrained('TypeEvenement');
            $table->string('Preside')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Evenement');
    }
}
