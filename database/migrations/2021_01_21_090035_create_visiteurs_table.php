<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisiteursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Visiteur', function (Blueprint $table) {
            $table->primary(['ClubId', 'PersonneId', 'EvenementId']);
            $table->foreignId('ClubId')->nullable()->constrained('Club');
            $table->foreignId('PersonneId')->nullable()->constrained('Personne');
            $table->foreignId('EvenementId')->constrained('Evenement');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Visiteur');
    }
}
