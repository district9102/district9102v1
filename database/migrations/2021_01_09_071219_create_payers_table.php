<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Payer', function (Blueprint $table) {
            $table->primary(['ClubId', 'FraisId']);
            $table->foreignId('ClubId')->nullable()->constrained('Club');
            $table->foreignId('FraisId')->nullable()->constrained('Frais');
            $table->foreignId('AnneeRotarienneId')->nullable()->constrained('AnneeRotarienne');
            $table->date('DatePayer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Payer');
    }
}
