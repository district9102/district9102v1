<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssocierDonEspecesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('AssocierDonEspece', function (Blueprint $table) {
            $table->primary(['EvenementId', 'DonEspeceId']);
            $table->foreignId('EvenementId')->constrained('Evenement');
            $table->foreignId('DonEspeceId')->nullable()->constrained('DonEspece');
            $table->foreignId('PersonneId')->nullable()->constrained('Personne');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('AssocierDonEspece');
    }
}
