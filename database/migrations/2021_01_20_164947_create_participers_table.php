<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Participer', function (Blueprint $table) {
            $table->primary(['PersonneId', 'EvenementId']);
            $table->foreignId('PersonneId')->constrained('Personne');
            $table->foreignId('EvenementId')->constrained('Evenement');
            $table->string('LibelleParticiper');
            $table->string('Tauxassiduite');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Participer');
    }
}
