<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Financement', function (Blueprint $table) {
            $table->primary(['EvenementId', 'TypeFinancementId']);
            $table->foreignId('EvenementId')->constrained('Evenement');
            $table->foreignId('TypeFinancementId')->nullable()->constrained('TypeFinancement');
            $table->decimal('Montant');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Financement');
    }
}
