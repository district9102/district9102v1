<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Membre', function (Blueprint $table) {
            $table->primary(['PersonneId', 'ClubId']);
            $table->foreignId('PersonneId')->constrained('Personne');
            $table->foreignId('ClubId')->constrained('Club');
            $table->foreignId('RoleId')->nullable()->constrained('Role');
            $table->date('DateAdhesion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Membre');
    }
}
