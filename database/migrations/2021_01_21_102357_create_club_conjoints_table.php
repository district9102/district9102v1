<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClubConjointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ClubConjoint', function (Blueprint $table) {
            $table->primary(['ClubId', 'EvenementId']);
            $table->foreignId('ClubId')->nullable()->constrained('Club');
            $table->foreignId('EvenementId')->constrained('Evenement');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ClubConjoint');
    }
}
