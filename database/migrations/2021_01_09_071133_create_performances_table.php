<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerformancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Performance', function (Blueprint $table) {
            $table->id();
            $table->string('EtatPerformance');
            $table->string('CodePerformance');
            $table->foreignId('PersonneId')->nullable()->constrained('Personne');
            $table->foreignId('AspectId')->nullable()->constrained('Aspect');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Performance');
    }
}
