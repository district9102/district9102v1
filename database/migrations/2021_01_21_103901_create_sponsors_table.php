<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSponsorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Sponsor', function (Blueprint $table) {
            $table->primary(['EvenementId', 'PartenaireId']);
            $table->foreignId('EvenementId')->constrained('Evenement');
            $table->foreignId('PartenaireId')->nullable()->constrained('Partenaire');
            $table->decimal('Montant');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Sponsor');
    }
}
