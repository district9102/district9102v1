<?php

use App\Http\Controllers\Bienvenu;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\participant;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\presentation;
use App\Http\Controllers\verification;
use App\Http\Controllers\renduassemble;
use App\Http\Controllers\ClubController;
use App\Http\Controllers\FraisController;
use App\Http\Controllers\AspectController;
use App\Http\Controllers\PaiementController;
use App\Http\Controllers\PersonneController;
use App\Http\Controllers\EvenementController;
use App\Http\Controllers\TableUserController;
use App\Http\Controllers\StatutUserController;
use App\Http\Controllers\AjoutMembreController;
use App\Http\Controllers\PerformanceController;
use App\Http\Controllers\AnneeRotarienneController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/* Route::get('/', function () {
    return view('welcomeadmin');
}); */

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('statut-user', StatutUserController::class);

//Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//User
Route::get('/ajoutMembre', [AjoutMembreController::class, 'indexAjoutMembre'])->name('ajoutMembreVue')->middleware('auth');
Route::post('/ajoutMembre',[AjoutMembreController::class, 'ajoutMembre'])->name('ajoutMembre');

Route::get('/tableUser', [TableUserController::class, 'indexTableUser'])->name('indextableUser')->middleware('auth');
Route::put('/supprimerUser/{id}', [TableUserController::class, 'supprimerUser'])->name('supprimerUser');
Route::post('/supprimerUserAll', [TableUserController::class, 'supprimerUserAll'])->name('supprimerUserAll');
Route::post('/supprimerUserAllCocher', [TableUserController::class, 'supprimerUserAllCocher'])->name('supprimerUserAllCocher');
Route::post('/retourAjouter', [TableUserController::class, 'retourAjouter'])->name('retourAjouter');
Route::post('/compteRenduReunion', [TableUserController::class, 'compteRenduReunion'])->name('compteRenduReunion');

Route::get('/evenementPublication', [EvenementController::class, 'evenement'])->name('evenement')->middleware('auth');
Route::post('/evenementPublication', [EvenementController::class, 'evenementPublication'])->name('evenementPublication');

Route::get('/fraisParticipation', [FraisController::class, 'fraisParticipation'])->name('fraisParticipation')->middleware('auth');
Route::post('/fraisParticipation', [FraisController::class, 'ValidationFraisParticipation'])->name('ValidationFraisParticipation');
Route::get('/paiement',[PaiementController::class, 'paiement'])->name('paiement');


//
Route::get('bienvenue', [Bienvenu::class, 'bienvenue']);
Route::get('create', [Bienvenu::class, 'frais']);
//Route::resource('frais', FraisController::class);
Route::resource('statut-user', StatutUserController::class)->middleware('auth');
Route::get('bienvenue', [Bienvenu::class, 'bienvenue'])->name('bienvenue')->middleware('auth');
Route::get('create', [Bienvenu::class, 'frais'])->middleware('auth');
//Route::resource('frais', FraisController::class)->middleware('auth');
Route::resource('frais', FraisController::class)->middleware('auth');
Route::post('personne/connexion', [PersonneController::class, 'verification'])->name('loginconnexion');
Route::resource('personne', PersonneController::class)/* ->middleware('auth') */;
Route::get('personne/desable/{id}', [PersonneController::class, 'disable'])->name('person.desable')->middleware('auth');
Route::resource('club', ClubController::class)/* ->middleware('auth') */;
Route::resource('anneerotarienne', AnneeRotarienneController::class)->middleware('auth');
Route::resource('performance', PerformanceController::class)->middleware('auth');
Route::resource('aspect', AspectController::class)->middleware('auth');
Route::get('rotary', [presentation::class, 'rotary']);
Route::get('interact', [presentation::class, 'interact']);
Route::get('district9102', [presentation::class, 'district9102']);
Route::get('ryla', [presentation::class, 'ryla']);
Route::get('acd', [presentation::class, 'acd']);
Route::post('seconnecter', [verification::class, 'verification'])->name('seconnecter');
Route::get('compterenduassemble', [renduassemble::class, 'create']);
Route::get('personnepresente', [participant::class, 'create']);
