@extends('layouts.dashboard')

@section('content')
@if (session()->has('successMessage'))
            <div class="alert alert-success" role="alert">
                {{ session('successMessage') }}
            </div>
        @endif
        @if (session()->has('errorMessage'))
            <div class="alert alert-danger" role="alert">
                {{ session('errorMessage') }}
            </div>
        @endif
<form class="form-horizontal row-fluid" action="{{route('frais.store')}}" method="POST">
    @csrf
    <div class="control-group">
        <label class="control-label" for="basicinput">Taxe : </label>
        <div class="controls">
            <div class="dropdown">
                <select id="libelle" name="libelle"
                    class="form-control select2  libelle @error('libelle') is-invalid @enderror" data-plugin="select2"
                    style="width: 25%">
                    <option value="">Veuillez selectionner</option>
                    @foreach ($TaxeList as $item)
                    <option value="{{ $item->id }}">
                        {{ $item->Libelle }}
                    </option>
                    @endforeach
                </select>
                @error('libelle') <span class="text-danger" role="alert"> {{ $message }}</span>
                @enderror

            </div>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="basicinput">Montant</label>
        <div class="controls">
            <div class="input-append">
                <input type="text" name="montant" placeholder="Montant " class="span8"><span class="add-on">$</span>
            </div>
        </div>
    </div>


    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn">Valider</button>
        </div>
    </div>
</form>

@endsection
