@extends('layouts.dashboard')

@section('content')
@if (session()->has('successMessage'))
            <div class="alert alert-success" role="alert">
                {{ session('successMessage') }}
            </div>
        @endif
        @if (session()->has('errorMessage'))
            <div class="alert alert-danger" role="alert">
                {{ session('errorMessage') }}
            </div>
        @endif
<form class="form-horizontal row-fluid" action="{{route('personne.store')}}" method="POST">
    @csrf
    <div class="control-group">
        <label class="control-label" for="basicinput">Date</label>
        <div class="controls">
            <input type="date" name="jour" id="basicinput"  class="span8" style="width: 50%" max="{{$date}}">
            <span class="help-inline"></span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Présidée par</label>
        <div class="controls">
            <div class="dropdown">
                <select id="preside" name="preside" style="width: 50%"
                    class="form-control select2  preside @error('preside') is-invalid @enderror" data-plugin="select2"
                    style="width: 25%">
                    <option value="">Veuillez selectionner</option>
                    @foreach ($listPreside as $item)
                    <option value="{{ $item->id }}">
                         {{$item->Nom}} {{$item->Prenom}}
                    </option>
                    @endforeach
                </select>
                @error('preside') <span class="text-danger" role="alert"> {{ $message }}</span>
                @enderror

            </div>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Heure début</label>
        <div class="controls">
            <input type="text" name="heureDebut1" id="basicinput"  class="span8" style="width:10%">
            <span class="help-inline">Heure (s)</span>
            <input type="text" name="heureDebut2" id="basicinput"  class="span8" style="width:10%">
            <span class="help-inline">Minute (s)</span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Heure fin</label>
        <div class="controls">
            <input type="text" name="heureFin1" id="basicinput"  class="span8" style="width:10%">
            <span class="help-inline">Heure (s)</span>
            <input type="text" name="heureFin2" id="basicinput"  class="span8" style="width:10%">
            <span class="help-inline">Minute (s)</span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Membres présents</label>
        <div class="controls">
            <input type="text" name="membrepresent" id="basicinput"  class="span8" style="width: 10%">
            <span class="help-inline">Numérique</span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Nombre de Rotariens du club parrain présent</label>
        <div class="controls">
            <input type="text" name="nbreparrainpresent" id="basicinput"  class="span8" style="width: 10%">
            <span class="help-inline">Numérique</span>
        </div>
    </div>
    {{-- <div class="control-group">
        <label class="control-label" for="basicinput">Quorum</label>
        <div class="controls">
            <label for="oui">Oui</label>
            <input type="radio" name="quorum" id="oui" value="oui">
            <label for="non">Non</label>
            <input type="radio" name="quorum" id="non" value="non">
        </div>
    </div> --}}
    <div class="control-group">
        <label class="control-label" for="basicinput">Grand point I</label>
        <div class="controls">
            <input type="text" name="gp1" id="basicinput"  class="span8" style="width: 50%">
            <span class="help-inline">Maximum 50 caractères</span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Synthèse des discussions point I</label>
        <div class="controls">
            <textarea name="synthese1" id="" cols="10" rows="10" style="margin: 0px; width: 328px; height: 40px;"></textarea>
            <span class="help-inline">Maximum 150 caractères</span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Grand point II</label>
        <div class="controls">
            <input type="text" name="gp2" id="basicinput"  class="span8" style="width: 50%">
            <span class="help-inline">Maximum 50 caractères</span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Synthèse des discussions point II</label>
        <div class="controls">
            <textarea name="synthese2" id="" cols="10" rows="10" style="margin: 0px; width: 328px; height: 40px;"></textarea>
            <span class="help-inline">Maximum 150 caractères</span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Grand point III</label>
        <div class="controls">
            <input type="text" name="gp3" id="basicinput"  class="span8" style="width: 50%">
            <span class="help-inline">Maximum 50 caractères</span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Synthèse des discussions point III</label>
        <div class="controls">
            <textarea name="synthese3" id="" cols="10" rows="10" style="margin: 0px; width: 328px; height: 40px;"></textarea>
            <span class="help-inline">Maximum 150 caractères</span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Grand point IV</label>
        <div class="controls">
            <input type="text" name="gp4" id="basicinput"  class="span8" style="width: 50%">
            <span class="help-inline">Maximum 50 caractères</span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Synthèse des discussions point IV</label>
        <div class="controls">
            <textarea name="synthese4" id="" cols="10" rows="10" style="margin: 0px; width: 328px; height: 40px;"></textarea>
            <span class="help-inline">Maximum 150 caractères</span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Grand point V</label>
        <div class="controls">
            <input type="text" name="gp5" id="basicinput"  class="span8" style="width: 50%">
            <span class="help-inline">Maximum 50 caractères</span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Synthèse des discussions point V</label>
        <div class="controls">
            <textarea name="synthese5" id="" cols="10" rows="10" style="margin: 0px; width: 328px; height: 40px;"></textarea>
            <span class="help-inline">Maximum 150 caractères</span>
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn" style="background: greenyellow">Valider</button>
        </div>
    </div>
</form>
@endsection
