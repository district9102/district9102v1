@extends('layouts.dashboard')

@section('content')
@if (session()->has('successMessage'))
            <div class="alert alert-success" role="alert">
                {{ session('successMessage') }}
            </div>
        @endif
        @if (session()->has('errorMessage'))
            <div class="alert alert-danger" role="alert">
                {{ session('errorMessage') }}
            </div>
        @endif
<form class="form-horizontal row-fluid" action="{{route('anneerotarienne.update', $listAnnee->id)}}" method="POST">
    @csrf
    @method('PUT')
    <div class="control">
        <label for="">Merci</label>
    </div>
    <div>
        <label class="control-label" for="basicinput"></label>
        <div class="controls">
            <span><b>Du : </b></span>
            <span style="margin-left:27%;"><b>Au :</b></span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Année rotarienne</label>
        <div class="controls">
            <input type="date" name="date1" id="basicinput" value="{{$listAnnee->Date1}}"  class="span8" style="width: 30%" >
            <input type="date" name="date2" id="basicinput" value="{{$listAnnee->Date2}}"  class="span8" style="width: 30%" >
        </div>
    </div>

    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn" style="background: greenyellow">Valider</button>
        </div>
    </div>
</form>
@endsection
