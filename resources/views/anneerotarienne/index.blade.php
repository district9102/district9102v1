@extends('layouts.dashboard')

@section('content')
<div class="module">
    <div class="module-head">
        <h3>
            Liste des personnes du club</h3>
    </div>
    <div class="module-body table">
        <table cellpadding="0" cellspacing="0" border="0"
            class="datatable-1 table table-bordered table-striped	 display" width="100%">
            <thead>
                <tr>
                    <th>
                        Date début période
                    </th>
                    <th>
                        Date fin période
                    </th>
                    <th style="text-align:right">

                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($listAnnee as $item)
                <tr class="odd gradeX">
                    <td>
                        {{ $item->Date1 }}
                    </td>
                    <td>
                        {{ $item->Date2 }}
                    </td>
                    <td style="text-align:right">
                        <a id="btnEdit"  title="Modifier" href="{{ route('anneerotarienne.edit', $item->id) }}"><i
                                class="fa fa-pencil text-default"></i></a>&nbsp; &nbsp; &nbsp;

                                <a id="btnDelete" title="Supprimer"
                                href="{{ route('anneerotarienne.destroy', $item->id) }}"><i class="fa fa-power-off" style="color: rgb(220,20,60)"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


@endsection
