@extends('layouts.dashboard')

@section('content')
<div class="module">
                                <div class="module-head">
                                    <h3>
                                        DataTables</h3>
                                </div>
                                <div class="module-body table">

                                        <form id="sup" class="lesform" method="post" action="{{ url('/supprimerUserAll') }}" >
                                            @csrf
                                            <button type="submit" class="btn btn-small btn-danger">Supprimer</button>
                                            <input  type="hidden"  name="idD" value="" id="idD" >
                                        </form>
                                        <form id="supAll" class="lesform" method="post" action="{{ url('/supprimerUserAllCocher') }}" >
                                            @csrf
                                            <button type="submit" class="btn btn-small btn-danger">Supprimer</button>
                                            <input  type="hidden"  name="idD2" value="" id="idD2" >

                                        </form>
                                    <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display"
                                        width="100%">
                                        <thead>
                                            <tr>
                                                <th >
                                                    <input onclick="checkAll(this)"  type="checkbox" >Tout corcher
                                                </th>
                                                <th>
                                                    Nom
                                                </th>
                                                <th>
                                                    Prenom
                                                </th>
                                                <th>
                                                    email
                                                </th>
                                                <th>
                                                    DateNaissance
                                                </th>
                                                <th>
                                                    Sexe
                                                </th>
                                                <th>
                                                    Action/Statut
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($ListeUser as $item)
                                             <tr class="odd gradeX">
                                                    <td>
                                                        @if($item->statutuser->CodeStatutUser == 0)
                                                        <input  onclick="setId(this)"  type="checkbox" class="AA" id="{{$item->id}}" name="horns" class="check" >
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{$item->Nom}}
                                                    </td>
                                                    <td>
                                                        {{$item->Prenom}}
                                                    </td>
                                                    <td>
                                                        {{$item->email}}
                                                    </td>
                                                    <td class="center">
                                                        {{$item->DateNaissance}}
                                                    </td>
                                                    <td class="center">
                                                        {{$item->Sexe}}
                                                    </td>
                                                    <td class="center">
                                                        @if($item->statutuser->CodeStatutUser == 0)
                                                            <form method="post" action="{{ url('/supprimerUser',$item->id) }}">
                                                                @csrf
                                                                @method('PUT')
                                                                <button type="submit" class="btn btn-small btn-danger">Supprimer</button>
                                                            </form >
                                                        @else
                                                            {{$item->statutuser->Libelle}}
                                                        @endif

                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th >
                                                    <input  type="checkbox" >Tout corcher
                                                </th>
                                                <th>
                                                    Nom
                                                </th>
                                                <th>
                                                    Prenom
                                                </th>
                                                <th>
                                                    email
                                                </th>
                                                <th>
                                                    DateNaissance
                                                </th>
                                                <th>
                                                    Sexe
                                                </th>
                                                <th>
                                                    Action/Statut
                                                </th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                        <form class="lesform" method="post" action="{{ url('/compteRenduReunion') }}" >
                                            @csrf
                                            <button type="submit" class="btn btn-small btn-info">Compte rendu de réunion</button>
                                            <input  type="hidden"  name="idD" value="" id="idD" >

                                        </form>
                                        <form class="lesform" method="post" action="" >
                                            @csrf
                                            <button type="submit" class="btn btn-small btn-info">Compte rendu d’action</button>
                                        </form>

                                        <form class="lesform" method="post" action="" >
                                            @csrf
                                            <button type="submit" class="btn btn-small btn-info">Taux mensuel d'assiduité </button>
                                            <input  type="hidden"  name="idD" value="" id="idD" >

                                        </form>
                                        <div id="la">
                                        <form class="lesforms" method="post" action="" >
                                            @csrf
                                            <button type="submit" class="btn btn-small btn-info">Taux mensuel de recouvrement des cotisations</button>
                                        </form>
                                        <form class="lesforms" method="post" action="" >
                                            @csrf
                                            <button type="submit" class="btn btn-small btn-info">Formulaire mensuel d’informations générales</button>
                                        </form>
                                        <form class="lesforms" method="post" action="" >
                                            @csrf
                                            <button type="submit" class="btn btn-small btn-info">Rapport trimestriel</button>
                                        </form>
                                        </div>
                                </div>
                            </div>


@endsection
