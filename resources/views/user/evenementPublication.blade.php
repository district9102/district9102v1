@extends('layouts.dashboard')

@section('content')

<div class="module">
							<div class="module-head">
								<h3>Forms</h3>
							</div>
							<div class="module-body">

									<form method="post" action="{{ url('/evenementPublication') }}" enctype="multipart/form-data" class="form-horizontal row-fluid">

                                        @csrf
                                        <div class="form-group">
											<label class="control-label" for="basicinput">Titre</label>
											<div class="controls">
												<input  type="text" name="Titre" value="{{ old('Titre')?? $iff['Titre']?? '' }}" id="basicinput" placeholder="Entrez le titre" class="span8">
                                                @if ($errors->has('Titre'))
                                                    <div class="error">
                                                         {{ $errors->first('Titre') }}
                                                    </div>
                                                @endif
											</div>
										</div><br>

                                        <div class="form-group">
											<label class="control-label" for="basicinput">Jour et Heure</label>
											<div class="controls">

												<input  type="datetime-local" name="JourHeure" value="{{ old('JourHeure')?? $iff['JourHeure']?? '' }}" id="basicinput" placeholder="" class="span8"  >
                                                @if ($errors->has('JourHeure'))
                                                    <div class="error">
                                                         {{ $errors->first('JourHeure') }}
                                                    </div>
                                                    @elseif(Session::has('error'))
                                                    <div class="error">
                                                        {{Session::get('error')}}
                                                    </div>
                                                @endif
											</div>
										</div><br>
                                        <div class="form-group">
											<label class="control-label" for="basicinput">Lieu de la Reunion</label>
											<div class="controls">
												<input  type="text" name="LieuReunion" value="{{ old('LieuReunion')?? $iff['LieuReunion']?? ''  }}" id="basicinput" placeholder="Entrez le lieu de la Reunion" class="span8">
                                                @if ($errors->has('LieuReunion'))
                                                    <div class="error">
                                                         {{ $errors->first('LieuReunion') }}
                                                    </div>

                                                @endif
											</div>
										</div><br>

                                         <div class="form-group">
											<label class="control-label" for="basicinput">Ville de l'evenement</label>
											<div class="controls">
												<input  type="text" name="VilleEvenement" value="{{ old('VilleEvenement')?? $iff['VilleEvenement']?? '' }}" id="basicinput" placeholder="Entrez la Ville de l'Evenement" class="span8">
                                                @if ($errors->has('VilleEvenement'))
                                                    <div class="error">
                                                         {{ $errors->first('VilleEvenement') }}
                                                    </div>

                                                @endif
											</div>
										</div><br>

                                         <div class="form-group">
                                            <label class="control-label" for="basicinput">PieceJointe</label>
                                            <div class="controls">
                                                <input  type="file" name="PieceJointe"  id="pieces"  value="{{ old('PieceJointe')?? $iff['PieceJointe']?? '' }}" />

                                                    @if ($errors->has('PieceJointe'))
                                                        <div class="error">
                                                            {{ $errors->first('PieceJointe') }}
                                                         </div>

                                                    @endif
                                            </div>
										</div><br>

                                        {{-- <div class="form-group">
											<label class="control-label" for="basicinput">Club</label>
											<div class="controls">
                                                <select id="" name="NomClub">
                                                    <option value="">Veuillez selectionner</option>
                                                    @foreach ($ClubList as $item)
                                                        <option value="{{ $item->NomClub }}">
                                                            {{ $item->NomClub}}
                                                        </option>
                                                    @endforeach
                                                </select>
											</div>
										</div><br> --}}

										<div class="form-group">
											<div class="controls">
												<button type="submit" class="btn btn-primary">Submit Form</button>
											</div>
										</div>
									</form>
							</div>
						</div>

@endsection

