@extends('layouts.dashboard')

@section('content')

<div class="module">
							<div class="module-head">
								<h3>Forms</h3>
							</div>
							<div class="module-body">

									<form method="post" action="{{ url('/ajoutMembre') }}" class="form-horizontal row-fluid">

                                        @csrf
										<div class="form-group">
											<label class="control-label" for="basicinput">Nom</label>
											<div class="controls">
												<input  type="text" name="Nom" value="{{ old('Nom') }}" id="basicinput" placeholder="Entrez votre nom" class="span8">
                                                 @if ($errors->has('Nom'))
                                                    <div class="error">
                                                         {{ $errors->first('Nom') }}
                                                    </div>
                                                @endif
											</div>
										</div><br>
                                        <div class="form-group">
											<label class="control-label" for="basicinput">Prenom</label>
											<div class="controls">
												<input  type="text" name="Prenom" value="{{ old('Prenom') }}" id="basicinput" placeholder="Entrez votre prénom" class="span8">
                                                @if ($errors->has('Prenom'))
                                                    <div class="error">
                                                         {{ $errors->first('Prenom') }}
                                                    </div>
                                                @endif
											</div>
										</div><br>

                                        <div class="form-group">
											<label class="control-label" for="basicinput">Email</label>
											<div class="controls">
												<input  type="email" name="email" value="{{ old('email') }}" id="basicinput" placeholder="Entrez votre adressemail" class="span8">
                                                @if ($errors->has('email'))
                                                    <div class="error">
                                                         {{ $errors->first('email') }}
                                                    </div>
                                                @endif
											</div>
										</div><br>
                                        <div class="form-group">
											<label class="control-label" for="basicinput">Date de Naissance</label>
											<div class="controls">

                                        <!--     @foreach ($errors->get('email') as $message)

                                            {{$message}}
    //
                    @endforeach -->
												<input  type="date" name="DateNaissance" value="{{ old('DateNaissance') }}" id="basicinput" placeholder="" class="span8"  >
                                                @if ($errors->has('DateNaissance'))
                                                    <div class="error">
                                                         {{ $errors->first('DateNaissance') }}
                                                    </div>
                                                @endif
											</div>
										</div><br>
                                        <div class="form-group">
											<label class="control-label" for="basicinput">Sexe</label>
											<div class="controls">
                                                <select name="Sexe" id="pet-select">
                                                    <option value="">Veuillez selectionner</option>
                                                    <option value="F ">F</option>
                                                    <option value="M">M</option>
                                                </select>
                                                @if ($errors->has('Sexe'))
                                                    <div class="error">
                                                         {{ $errors->first('Sexe') }}
                                                    </div>
                                                @endif
											</div>
                                        </div><br>
                                        <div class="form-group">
											<label class="control-label" for="basicinput">Role</label>
											<div class="controls">
                                                <select id="" name="Role">
                                                    <option value="">Veuillez selectionner</option>
                                                    @foreach ($RoleListe as $item)
                                                        <option value="{{ $item->id}}">
                                                            {{ $item->LibelleRole}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('Role'))
                                                    <div class="error">
                                                         {{ $errors->first('Role') }}
                                                    </div>
                                                @endif
											</div>
										</div><br>
                                        <div class="form-group">
											<label class="control-label" for="basicinput">Mot de passe</label>
											<div class="controls">
												<input  type="password" name="password" value="{{ old('password') }}" id="basicinput" placeholder="Entrez votre mot de passe" class="span8">
                                                @if ($errors->has('password'))
                                                    <div class="error">
                                                         {{ $errors->first('password') }}
                                                    </div>
                                                @endif
											</div>
										</div><br>
                                        <div class="form-group">
											<label class="control-label" for="basicinput">Confirmer mot de passe</label>
											<div class="controls">
												<input  type="password" name="confirmpassword" value="{{ old('confirmpassword') }}" id="basicinput" placeholder="Entrez votre mot de passe" class="span8">
                                                @if ($errors->has('confirmpassword'))
                                                    <div class="error">
                                                         {{ $errors->first('confirmpassword') }}
                                                    </div>
                                                @endif
											</div>
										</div><br>

										<div class="form-group">
											<div class="controls">
												<button type="submit" class="btn btn-primary">Submit Form</button>
											</div>
										</div>
									</form>
							</div>
						</div>

@endsection
