@extends('layouts.dashboard')

@section('content')

<div class="module">
							<div class="module-head">
								<h3>Forms</h3>
							</div>
							<div class="module-body">

									<form method="post" action="{{ url('/fraisParticipation') }}" class="form-horizontal row-fluid">

                                        @csrf
										{{-- <div class="form-group">
											<label class="control-label" for="basicinput">Club</label>
											<div class="controls">
                                                <select id="" name="NomClub">
                                                    <option value="">Veuillez selectionner</option>
                                                    @foreach ($ClubList as $item)
                                                        <option value="{{ $item->id }}">
                                                            {{ $item->NomClub}}
                                                        </option>
                                                    @endforeach
                                                </select>
											</div>
										</div><br> --}}
                                        <div class="form-group">
											<label class="control-label" for="basicinput">Type de frais</label>
											<div class="controls">
                                                <select id="" name="TypeFrais">
                                                    <option value="">Veuillez selectionner</option>
                                                    @foreach ($TypeFrais as $item)
                                                        <option value="{{ $item->id }}">
                                                            {{ $item->Libelle}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('TypeFrais'))
                                                    <div class="error">
                                                         {{ $errors->first('TypeFrais') }}
                                                    </div>
                                                @endif
											</div>
                                        </div><br>

                                        <div class="form-group">
											<label class="control-label" for="basicinput">Année Rotarienne</label>
											<div class="controls">
                                                <select id="" name="AnneeRotarienneListe">
                                                    <option value="">Veuillez selectionner</option>
                                                    @foreach ($AnneeRotarienneListe as $item)
                                                        <option value="{{ $item->Date1 }}/{{ $item->Date2 }}">
                                                            {{ $item->Date1 }}/{{ $item->Date2 }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                 @if ($errors->has('AnneeRotarienneListe'))
                                                    <div class="error">
                                                         {{ $errors->first('AnneeRotarienneListe') }}
                                                    </div>
                                                @endif
											</div>
										</div><br>

                                        <div class="form-group">
											<label class="control-label" for="basicinput">Montant</label>
											<div class="controls">
												<input  type = "number" step = "any" min="0" name="Montant" value="{{ old('Montant') }}" id="basicinput" placeholder="Entrez le montant" class="span8">
                                                @if ($errors->has('Montant'))
                                                    <div class="error">
                                                         {{ $errors->first('Montant') }}
                                                    </div>
                                                @endif
											</div>
										</div><br>

                                    
											</div>
										</div><br>
										<div class="form-group">
											<div class="controls">
												<button type="submit" class="btn btn-primary">Submit Form</button>
											</div>
										</div>
									</form>
							</div>
						</div>

@endsection
