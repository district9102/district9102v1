@extends('layouts.dashboard')

@section('content')
<div class="module">
    <div class="module-head">
        <h3>
            Liste des personnes du club</h3>
    </div>
    <div class="module-body table">
        <table cellpadding="0" cellspacing="0" border="0"
            class="datatable-1 table table-bordered table-striped	 display" width="100%">
            <thead>
                <tr>
                    <th>
                        Nom
                    </th>
                    <th>
                        Prénom
                    </th>
                    <th>
                        Sexe
                    </th>
                    <th>
                        Email
                    </th>
                    <th>
                        Date de naissance
                    </th>
                    <th>

                    </th>
                    <th>

                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($listPersonne as $item)
                <tr class="odd gradeX">
                    <td>
                        {{ $item->Nom }}
                    </td>
                    <td>
                        {{ $item->Prenom }}
                    </td>
                    <td>
                        {{ $item->Sexe }}
                    </td>
                    <td class="center">
                        {{ $item->email }}
                    </td>
                    <td class="center">
                        {{ $item->DateNaissance }}
                    </td>
                    <td >
                        @if ($item->statutuser->CodeStatutUser == 0)
                        <a id="btnEdit"  title="Modifier" ><i class="fa fa-circle" style="color: green"></i></a>
                        @else
                        <a id="btnEdit"  title="Modifier" ><i class="fa fa-circle" style="color: red"></i></a>
                        @endif
                        {{-- <a id="btnEdit"  title="Modifier" ><i class="fa fa-circle" style="color: red"></i></a>
                        <a id="btnEdit"  title="Modifier" ><i class="fa fa-circle" style="color: green"></i></a> --}}
                    </td>
                    <td>
                        <a id="btnEdit"  title="Modifier" href="{{ route('personne.edit', $item->id) }}"><i
                                class="fa fa-pencil text-default"></i></a>&nbsp; &nbsp; &nbsp;

                        <a id="btnDelete" title="Supprimer"
                            href="{{route('person.desable', ['id' =>$item->id]) }}"><i class="fa fa-power-off" style="color: rgb(220,20,60)"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


@endsection
