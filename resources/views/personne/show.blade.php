@extends('layouts.dashboard')

@section('content')
@if (session()->has('successMessage'))
            <div class="alert alert-success" role="alert">
                {{ session('successMessage') }}
            </div>
        @endif
        @if (session()->has('errorMessage'))
            <div class="alert alert-danger" role="alert">
                {{ session('errorMessage') }}
            </div>
        @endif
<form class="form-horizontal row-fluid" action="{{route('personne.update', $listPersonne->id)}}" method="POST">
    @csrf
    @method('PUT')
    <div class="control-group">
        <label class="control-label" for="basicinput">Nom</label>
        <div class="controls">
            <input type="text" name="nom" id="basicinput" value="{{$listPersonne->Nom}}"  class="span8" style="width: 50%">
            <span class="help-inline">Maximum 20 caractères</span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Prénom</label>
        <div class="controls">
            <input type="text" name="prenom" id="basicinput" value="{{$listPersonne->Prenom}}"  class="span8" style="width: 50%">
            <span class="help-inline">Maximum 20 caractères</span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Date de naissance</label>
        <div class="controls">
            <input type="date" name="datenaissance" id="basicinput" value="{{$listPersonne->DateNaissance}}"  class="span8" style="width: 50%" >
            <span class="help-inline"></span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Email</label>
        <div class="controls">
            <input type="email" name="email" value="{{$listPersonne->email}}" id="basicinput"  class="span8" style="width: 50%">
            <span class="help-inline"></span>
        </div>
        @error('email') <span class="text-danger" role="alert"> {{ $message }}</span>
                @enderror
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Sexe : </label>
        <div class="controls">
            <div class="dropdown">
                <select id="sexe" name="sexe"
                    class="form-control select2  sexe @error('sexe') is-invalid @enderror" data-plugin="select2"
                    style="width: 50%">
                    <option value="">Veuillez selectionner le sexe</option>
                    <option value="M">Masculin | M</option>
                    <option value="F">Féminin | F</option>
                </select>
                @error('libelle') <span class="text-danger" role="alert"> {{ $message }}</span>
                @enderror

            </div>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Statut de la personne : </label>
        <div class="controls">
            <div class="dropdown">
                <select id="libelle" name="libelle"
                    class="form-control select2  libelle @error('libelle') is-invalid @enderror" data-plugin="select2"
                    style="width: 25%">
                    <option value="">Veuillez selectionner</option>
                    @foreach ($listStatut as $item)
                    <option value="{{ $item->id }}">
                        {{ $item->Libelle }}
                    </option>
                    @endforeach
                </select>
                @error('libelle') <span class="text-danger" role="alert"> {{ $message }}</span>
                @enderror

            </div>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Mot de passe</label>
        <div class="controls">
            <input type="text" name="password" value="{{$listPersonne->password}}" id="basicinput"  class="span8" style="width: 50%">
            <span class="help-inline">Minimum 8 caractères</span>
        </div>
    </div>

    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn" style="background: greenyellow">Valider</button>
        </div>
    </div>
</form>
@endsection
