@extends('layouts.dashboard')

@section('content')
@if (session()->has('successMessage'))
            <div class="alert alert-success" role="alert">
                {{ session('successMessage') }}
            </div>
        @endif
        @if (session()->has('errorMessage'))
            <div class="alert alert-danger" role="alert">
                {{ session('errorMessage') }}
            </div>
        @endif
<form class="form-horizontal row-fluid" action="{{route('club.store')}}" method="POST">
    @csrf

    <div class="control-group">
        <label class="control-label" for="basicinput">Liste des présences</label>
        <div class="controls">
            <div class="dropdown">
                <select id="nomResponsable" name="nomResponsable" style="width: 50%"
                    class="form-control select2  nomResponsable @error('nomResponsable') is-invalid @enderror" data-plugin="select2"
                    style="width: 25%">
                    <option value="">Veuillez selectionner</option>
                    @foreach ($personneList as $item)
                    <option value="{{ $item->id }}">
                        {{ $item->Prenom }} {{$item->Nom}}
                    </option>
                    @endforeach
                </select>
                @error('nomResponsable') <span class="text-danger" role="alert"> {{ $message }}</span>
                @enderror

            </div>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Evenement concerné</label>
        <div class="controls">
            <div class="dropdown">
                <select id="nomEvenement" name="nomEvenement" style="width: 50%"
                    class="form-control select2  nomEvenement @error('nomEvenement') is-invalid @enderror" data-plugin="select2"
                    style="width: 25%">
                    <option value="">Veuillez selectionner</option>
                    @foreach ($evenementList as $item)
                    <option value="{{ $item->id }}">
                        {{ $item->Titre }}
                    </option>
                    @endforeach
                </select>
                @error('nomEvenement') <span class="text-danger" role="alert"> {{ $message }}</span>
                @enderror

            </div>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Nom</label>
        <div class="controls">
            <select id="nomResponsable" name="nomResponsable" style="width: 50%"
                    class="form-control select2  nomResponsable @error('nomResponsable') is-invalid @enderror" data-plugin="select2"
                    style="width: 25%">
                    <option value="">Veuillez selectionner</option>
                    @foreach ($personneList as $item)
                    <option value="{{ $item->id }}">
                        {{ $item->Prenom }} {{$item->Nom}}
                    </option>
                    @endforeach
                </select>
            <span class="help-inline">Maximum 20 caractères</span>
        </div>
        @error('libelleParticiper') <span class="text-danger">{{ $errors->first('libelleParticiper') }}</span> @enderror
        <input type="text" name="">
    </div>


    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn" style="background: greenyellow">Valider</button>
        </div>
    </div>
</form>
<script>

</script>
@endsection
