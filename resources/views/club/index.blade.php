@extends('layouts.dashboard')

@section('content')
<div class="module">
    <div class="module-head">
        <h3>
            Liste des clubs</h3>
    </div>
    <div class="module-body table">
        <table cellpadding="0" cellspacing="0" border="0"
            class="datatable-1 table table-bordered table-striped	 display" width="100%">
            <thead>
                <tr>
                    <th>
                        Club
                    </th>
                    <th>
                        Responsable
                    </th>
                    <th>
                        Historique
                    </th>
                    <th>
                        Action FM
                    </th>
                    <th>
                        Pays
                    </th>
                    <th>

                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($listClub as $item)
                <tr class="odd gradeX">
                    <td>
                        {{ $item->NomClub }}
                    </td>
                    <td>
                        {{ $item->personneresponsable->Nom }} {{ $item->personneresponsable->Prenom }}
                    </td>
                    <td>
                        {{ $item->Historique }}
                    </td>
                    <td class="center">
                        {{ $item->ActionFM }}
                    </td>
                    <td class="center">
                        {{ $item->pays->Intitule }}
                    </td>
                    <td>
                        <a id="btnEdit"  title="Modifier" href="{{ route('club.edit', $item->id) }}"><i
                                class="fa fa-pencil text-default"></i></a>&nbsp; &nbsp; &nbsp;

                        <a id="btnDelete" title="Supprimer"
                            href="{{ $item->id }}"><i class="fa fa-power-off" style="color: rgb(220,20,60)"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


@endsection
