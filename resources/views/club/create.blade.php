@extends('layouts.dashboard')

@section('content')
@if (session()->has('successMessage'))
            <div class="alert alert-success" role="alert">
                {{ session('successMessage') }}
            </div>
        @endif
        @if (session()->has('errorMessage'))
            <div class="alert alert-danger" role="alert">
                {{ session('errorMessage') }}
            </div>
        @endif
<form class="form-horizontal row-fluid" action="{{route('club.store')}}" method="POST">
    @csrf
    <div class="control-group">
        <label class="control-label" for="basicinput">Nom du club</label>
        <div class="controls">
            <input type="text" name="nomclub" id="basicinput"  class="span8" style="width: 50%">
            <span class="help-inline">Maximum 20 caractères</span>
        </div>
        @error('nomclub') <span class="text-danger">{{ $errors->first('nomclub') }}</span> @enderror
        <span class="focus-input100"></span>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Responsable du club : </label>
        <div class="controls">
            <div class="dropdown">
                <select id="nomResponsable" name="nomResponsable" style="width: 50%"
                    class="form-control select2  nomResponsable @error('nomResponsable') is-invalid @enderror" data-plugin="select2"
                    style="width: 25%">
                    <option value="">Veuillez selectionner le responsable</option>
                    @foreach ($personneList as $item)
                    <option value="{{ $item->id }}">
                        {{ $item->Prenom }} {{$item->Nom}}
                    </option>
                    @endforeach
                </select>
                @error('nomResponsable') <span class="text-danger" role="alert"> {{ $message }}</span>
                @enderror

            </div>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Catégorie : </label>
        <div class="controls">
            <div class="dropdown">
                <select id="nomCategorie" name="nomCategorie" style="width: 50%"
                    class="form-control select2  nomCategorie @error('nomCategorie') is-invalid @enderror" data-plugin="select2"
                    style="width: 25%">
                    <option value="">Veuillez selectionner la catégorie</option>
                    @foreach ($categorieList as $item)
                    <option value="{{ $item->id }}">
                        {{ $item->Libelle }}
                    </option>
                    @endforeach
                </select>
                @error('nomCategorie') <span class="text-danger" role="alert"> {{ $message }}</span>
                @enderror

            </div>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Pays</label>
        <div class="controls">
            <div class="dropdown">
                <select id="nomPays" name="nomPays" style="width: 50%"
                    class="form-control select2  nomPays @error('nomPays') is-invalid @enderror" data-plugin="select2"
                    style="width: 25%">
                    <option value="">Veuillez selectionner le pays</option>
                    @foreach ($paysList as $item)
                    <option value="{{ $item->id }}">
                        {{ $item->Intitule }}
                    </option>
                    @endforeach
                </select>
                @error('nomPays') <span class="text-danger" role="alert"> {{ $message }}</span>
                @enderror

            </div>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Action FM</label>
        <div class="controls">
            <input type="text" name="actionfm" id="basicinput"  class="span8" style="width: 50%">
            <span class="help-inline">Maximum 50 caractères</span>
        </div>
        @error('nomclub') <span class="text-danger">{{ $errors->first('nomclub') }}</span> @enderror
        <span class="focus-input100"></span>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Historique</label>
        <div class="controls">
            <textarea name="historique" id="historique" cols="10" rows="10" style="margin: 0px; height: 39px; width: 331px;"></textarea>
            <span class="help-inline">Maximum 50 caractères</span>
        </div>
        @error('nomclub') <span class="text-danger">{{ $errors->first('nomclub') }}</span> @enderror
        <span class="focus-input100"></span>
    </div>


    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn" style="background: greenyellow">Valider</button>
        </div>
    </div>
</form>
<script>

</script>
@endsection
