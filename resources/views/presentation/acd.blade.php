@extends('layouts.adminpresentation')

@section('content')
<section id="breadcrumbs" class="breadcrumbs">
    <div class="container">

        <div class="d-flex justify-content-between align-items-center">
            <h2>ACD</h2>
            <ol>
                <li><a href="index.html">Accueil</a></li>
                <li>ACD</li>
            </ol>
        </div>

    </div>
</section><!-- End Breadcrumbs -->
<section id="about-us" class="about-us">
    <div class="container" data-aos="fade-up">

        
        <h5>ASSEMBLÉE ET CONFÉRENCE DE DISTRICT</h5>
        <p>
            L’objectif de l’Assemblée et Conférence de District (ACD) est d’engager Rotaractiens et Interactiens de tout
            statut, du dirigeant aux nouveaux membres des clubs.

            Lors de l’ACD, les actions et projets réalisés par les membres et clubs seront reconnus afin d’inspirer les
            membres du district et promouvoir la vision et mission du district 9102 du Rotary International au delà du
            niveau des clubs et de fournir d’agréables expériences de camaraderie.

            Le Représentant Rotaract du District (RRD) est dans l’attente de recevoir tous les Rotaractiens et
            Interactiens du Rotary International District 9102, d’autres Rotariens, partenaires en service et invités.
        </p>

    </div>
</section><!-- End About Us Section -->


@endsection
