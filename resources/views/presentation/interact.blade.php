@extends('layouts.adminpresentation')

@section('content')
<section id="breadcrumbs" class="breadcrumbs">
    <div class="container">

        <div class="d-flex justify-content-between align-items-center">
            <h2>ROTARACT</h2>
            <ol>
                <li><a href="index.html">Accueil</a></li>
                <li>ROTARACT</li>
            </ol>
        </div>

    </div>
</section><!-- End Breadcrumbs -->
<section id="about-us" class="about-us">
    <div class="container" data-aos="fade-up">

        <div class="row content">
            <div class="col-lg-6 pt-4 pt-lg-0" data-aos="fade-left">
                <h5>
                    QUI SOMMES NOUS?
                </h5>

                <p>
                    internationale et faites-vous de nouveaux amis dans le monde entier.
                    Les clubs Interact rassemblent des jeunes de 12 à 18 ans pour développer leurs compétences tout en
                    découvrant le pouvoir du service rendu aux autres. Découvrez à quel point un leadership sérieux peut
                    être vraiment amusant.

                </p>

                <h5>
                    QUELS SONT LES BENEFICES?
                </h5>
                <p>
                    Connectez-vous avec les leaders de votre communauté et du monde entier pour:
                </p>
                <ul>
                    <li><i class="ri-check-double-line"></i> Agissez pour faire une différence dans votre école et votre
                        communauté;</li>
                    <li><i class="ri-check-double-line"></i> Découvrir de nouvelles cultures et promouvoir la
                        compréhension internationale;</li>
                    <li><i class="ri-check-double-line"></i> Devenir un leader dans votre école et votre
                        communauté;quat. Duis aute irure dolor in reprehenderit in</li>
                    <li><i class="ri-check-double-line"></i> Amusez-vous et faites-vous de nouveaux amis du monde
                        entier.</li>
                </ul>
            </div>

            <div class="col-lg-6 pt-4 pt-lg-0" data-aos="fade-left">
                <h5>
                    QUE FAISONS NOUS?
                </h5>

                <p>
                    Les clubs interagissent avec moins de projets chaque année, avec leur école ou leur communauté, et favorisent la compréhension internationale. Les clubs Rotary parrainent et guident les intervenants dans l’exécution de projets et le développement du leadership.
Célébrez l'impact global de l'interaction en s'impliquant dans:

                </p>
                <ul>
                    <li><i class="ri-check-double-line"></i> Semaine d'interaction mondiale;</li>
                    <li><i class="ri-check-double-line"></i> Prix vidéo Interact;</li>
                    <li><i class="ri-check-double-line"></i> Journée de la jeunesse du Rotary aux Nations Unies;</li>
                    <li><i class="ri-check-double-line"></i> Journée mondiale du service de la jeunesse.</li>
                </ul>
            </div>
        </div>

    </div>
</section><!-- End About Us Section -->


@endsection
