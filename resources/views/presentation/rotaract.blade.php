@extends('layouts.adminpresentation')

@section('content')
<section id="breadcrumbs" class="breadcrumbs">
    <div class="container">

        <div class="d-flex justify-content-between align-items-center">
            <h2>ROTARACT</h2>
            <ol>
                <li><a href="index.html">Accueil</a></li>
                <li>ROTARACT</li>
            </ol>
        </div>

    </div>
</section><!-- End Breadcrumbs -->
<section id="about-us" class="about-us">
    <div class="container" data-aos="fade-up">

        <div class="row content">
            <div class="col-lg-6" data-aos="fade-right">
                <h5>DISTRICT DE ROTARACT 9102</h5>
                <p>
                    <b>Le district 9100 du Rotary International</b> , qui comprend 14 pays d’Afrique de l’Ouest (Bénin, Burkina
                    Faso, Cap-Vert, Côte d’Ivoire, Gambie, Guinée Bissau, Guinée, Ghana, Libéria, Mali, Niger, Sierra
                    Leone, Sénégal et Togo) et a reconnu comme le plus grand district géographique du Rotary World. Il a
                    été créé en 1985 et utilisait trois langues officielles: l'anglais, le français et le portugais.

                    Pour une administration efficace, les 34 000 clubs Rotary du monde entier sont regroupés sous
                    Districts par Rotary International sur la base d’un nombre minimum de clubs et de membres Rotary. Un
                    district est sous la direction et la supervision d'un officier élu, le gouverneur de district. Au
                    fil des ans, il est devenu évident qu'un seul gouverneur ne pouvait pas superviser efficacement tous
                    les clubs dans une zone géographique aussi vaste, comme le district 9100, alors que le nombre de
                    clubs et les moyens de transport en Afrique augmentaient. de l'Ouest avait aggravé la situation.
                </p>
            </div>
            <div class="col-lg-6 pt-4 pt-lg-0" data-aos="fade-left">
                <p>
                    En mars 2011, le Rotary International a pris la décision audacieuse de changer de district en
                    divisant le district de Rotary 9100 en deux districts distincts, à savoir le Rotary District
                    International 9101, comprenant le Burkina Faso, le Cap Vert, la Côte d'Ivoire, la Gambie, la Guinée
                    Bissau, la Guinée et le Libéria. . Mali, Sierra Leone et Sénégal; ainsi que le Rotary International
                    District 9102 composé du Bénin, du Ghana, du Niger et du Togo. Chacun de ces districts nouvellement
                    créés sera sous la direction et la supervision d'un gouverneur de district à compter du 1er juillet
                    2013.

                    Le district est géré chaque année rotarienne par un représentant de district Rotaract (RDR) du pays
                    du gouverneur de district et par un comité de district.
                    Le district 9102 de Rotaract compte plus de 900 rotariens et plus de 100 Interactors dans 56 clubs
                    dans les quatre pays.
                </p>
            </div>
        </div>

    </div>
</section><!-- End About Us Section -->


@endsection
