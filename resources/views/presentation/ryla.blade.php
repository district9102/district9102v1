@extends('layouts.adminpresentation')

@section('content')
<section id="breadcrumbs" class="breadcrumbs">
    <div class="container">

        <div class="d-flex justify-content-between align-items-center">
            <h2>RYLA</h2>
            <ol>
                <li><a href="index.html">Accueil</a></li>
                <li>RYLA</li>
            </ol>
        </div>

    </div>
</section><!-- End Breadcrumbs -->
<section id="about-us" class="about-us">
    <div class="container" data-aos="fade-up">

        <div class="row content">
            <div class="col-lg-6 pt-4 pt-lg-0" data-aos="fade-left">
                <h5>
                    ROTARY YOUTH LEADERSHIP AWARDS
                </h5>
                <h6>
                    Avez-vous le nécessaire pour devenir un leader dynamique avec une vision de changer le monde que de
                    changer soi?
                </h6>

                <p>
                    Rotary Youth Leadership Awards (RYLA) est une intense expérience de leadership organisé par les
                    clubs Rotary des différents districts ; par lequel vous pouvez développer vos compétences
                    professionnels et créer des relations amicales.

                </p>

                <h5>
                    Quels sont les avantages?
                </h5>
                <p>
                    Connecter avec des leaders dans votre communauté et partout dans le monde:
                </p>
                <ul>
                    <li><i class="ri-check-double-line"></i> Créer des compétences de communications dynamiques</li>
                    <li><i class="ri-check-double-line"></i> Découvrir de nouvelles stratégies pour devenir un leader
                        dynamique dans votre école ou communauté</li>
                    <li><i class="ri-check-double-line"></i> Apprendre des leaders de communauté, des orateurs
                        inspirants et conseillers.</li>
                    <li><i class="ri-check-double-line"></i> Révéler votre potentiel de tourner motivation en action.
                    </li>
                    <li><i class="ri-check-double-line"></i> Créer des liens amicaux durables.</li>
                </ul>
            </div>

            <div class="col-lg-6 pt-4 pt-lg-0" data-aos="fade-left">
                <h5>
                    Quelles sont les implications?
                </h5>

                <p>
                    RYLA est organisé localement par les clubs Rotary et districts pour les participants âgés de 14 à 30
                    ans. Dépendant des besoins de la communauté, RYLA peut prendre la forme d’un séminaire organisé en
                    une journée, ou pendant un retrait de 3 jours ou en un camp d’une semaine. L’évènement dure
                    davantage pendant 3-10 jours y compris des présentations, activités et séminaires couvrant une
                    variété de sujets.
                    Votre communauté doit organiser cet évènement pour les élèves du lycée afin d’aiguiser leur
                    potentiel en leadership. Et pour les étudiants à l’université, de pouvoir résoudre des problèmes
                    avec un esprit créatif.

                </p>

                <h5>
                    Comment y participer
                </h5>
                <p>
                    Les participants du Leadership Award (RYLA) sont nominés par les clubs Rotary locaux. Veuillez
                    contacter votre club Rotary pour d’amples informations sur l’évènement dans votre milieu, comment
                    postuler, et les frais à payer.

                    Etes-vous membre ou ancien membre de l’Interact, Rotaract ou d’un programme d’échange du Rotary ?
                    RYLA pourrait vous aider à vous reconnecter au Rotary. Prenez- en- part localement, régionalement et
                    sur le plan international pour pouvoir promouvoir vos compétences ou accepter le bénévolat en tant
                    que conseiller.
                </p>
            </div>
            <h5>Voulez-vous organiser RYLA</h5>
            <p>
                Stimuler la génération prochaine des leaders de communauté par le Rotary Youth Leadership Awards (RYLA).
                Cet évènement n’est pas organisé juste pour développer le potentiel des jeunes gens, mais ils les aident
                aussi à inspirer et les défi à devenir des leaders influents de communauté.
            </p>
        </div>

    </div>
</section><!-- End About Us Section -->


@endsection
