@extends('layouts.adminpresentation')

@section('content')
<section id="breadcrumbs" class="breadcrumbs">
    <div class="container">

        <div class="d-flex justify-content-between align-items-center">
            <h2>DISTRICT9102</h2>
            <ol>
                <li><a href="index.html">Accueil</a></li>
                <li>DISTRICT9102</li>
            </ol>
        </div>

    </div>
</section><!-- End Breadcrumbs -->
<section id="about-us" class="about-us">
    <div class="container" data-aos="fade-up">

        <div class="section-title">
            <h2>DISTRICT9102</strong></h2>
        </div>

        <div class="row content">
            <div class="col-lg-6" data-aos="fade-right">
                <p>
                    <b>Le district 9100</b> du Rotary International qui comprenait 14 pays d’Afrique de l’Ouest
                    –
                    (Bénin, Burkina Faso, Cap Vert, Côte d’Ivoire, Gambie, Guinée Bissau, Guinée, Ghana,
                    Libéria, Mali, Niger, Sierra Leone, Sénégal et Togo) et a été reconnu comme le plus grand
                    district géographique du monde Rotary. Il a été créé en 1985 et utilisait trois langues
                    officielles: l’Anglais, le Français et le Portugais.

                    Pour l’efficacité de l’administration, les 34 000 clubs Rotary du monde entier sont
                    regroupés sous Districts par Rotary International sur la base d’un nombre minimum de clubs
                    et de membres Rotary. Un district est sous la direction et la supervision d’un officier élu,
                    le gouverneur de district. Au fil des années, il est devenu évident qu’un seul gouverneur ne
                    pouvait pas superviser efficacement tous les clubs dans une zone géographique aussi vaste,
                    comme le district 9100, car le nombre de clubs ne cessait de croître et les moyens de
                    déplacement en Afrique de l’Ouest avaient aggravé la situation.
                </p>
            </div>
            <div class="col-lg-6 pt-4 pt-lg-0" data-aos="fade-left">
                <p>
                    En Mars 2011, le Rotary International a pris la décision audacieuse de changer de district
                    en divisant Ie district 9100 du Rotary en deux districts distincts, à savoir les districts
                    de Rotary Internationale 9101, comprenant le Burkina Faso, le Cap-Vert, la Côte d’Ivoire, la
                    Gambie, la Guinée Bissau, la Guinée et le Libéria. , Mali, Sierra Leone et Sénégal; ainsi
                    que le district 9102 du Rotary International composé du Bénin, du Ghana, du Niger et du
                    Togo. Chacun de ces districts nouvellement créés sera placé sous la direction et la
                    supervision d’un gouverneur de district à compter du 1er juillet 2013.

                    Le district est géré par un représentant Rotaract (DRR) du pays du gouverneur de district et
                    par un comité de district. Le Rotaract District 9102 compte plus de 900 Rotaractiens et plus
                    de 100 Interacteurs dans 71 clubs Rotaract dans les quatre pays.
                </p>
            </div>
        </div>

    </div>
</section><!-- End About Us Section -->

@endsection
