@extends('layouts.dashboard')

@section('content')
<div class="module">
    <div class="module-head">
        <h3>
            Liste des aspects</h3>
    </div>
    <div class="module-body table">
        <table cellpadding="0" cellspacing="0" border="0"
            class="datatable-1 table table-bordered table-striped	 display" width="100%">
            <thead>
                <tr>
                    <th>
                        Aspect
                    </th>
                    <th>
                        Code
                    </th>
                    <th>

                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($listAspect as $item)
                <tr class="odd gradeX">
                    <td>
                        {{ $item->Libelle }}
                    </td>
                    <td>
                        {{ $item->CodeAspect }}
                    </td>
                    <td style="text-align:right">
                        <a id="btnEdit"  title="Modifier" href="{{ route('aspect.edit', $item->id) }}"><i
                                class="fa fa-pencil text-default"></i></a>&nbsp; &nbsp; &nbsp;

                        <a id="btnDelete" title="Supprimer"
                            href="{{route('person.desable', ['id' =>$item->id]) }}"><i class="fa fa-power-off" style="color: rgb(220,20,60)"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


@endsection
