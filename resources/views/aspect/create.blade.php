@extends('layouts.dashboard')

@section('content')
@if (session()->has('successMessage'))
            <div class="alert alert-success" role="alert">
                {{ session('successMessage') }}
            </div>
        @endif
        @if (session()->has('errorMessage'))
            <div class="alert alert-danger" role="alert">
                {{ session('errorMessage') }}
            </div>
        @endif
<form class="form-horizontal row-fluid" action="{{route('aspect.store')}}" method="POST">
    @csrf
    <div class="control-group">
        <label class="control-label" for="basicinput">Aspect</label>
        <div class="controls">
            <input type="text" name="libelle" id="basicinput"  class="span8" style="width: 50%">
            <span class="help-inline">Maximum 20 caractères</span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Code</label>
        <div class="controls">
            <input type="text" name="code" id="basicinput"  class="span8" style="width: 50%">
            <span class="help-inline">Numérique</span>
        </div>
    </div>


    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn" style="background: greenyellow">Valider</button>
        </div>
    </div>
</form>
@endsection
