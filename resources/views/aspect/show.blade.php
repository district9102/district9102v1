@extends('layouts.dashboard')

@section('content')
@if (session()->has('successMessage'))
            <div class="alert alert-success" role="alert">
                {{ session('successMessage') }}
            </div>
        @endif
        @if (session()->has('errorMessage'))
            <div class="alert alert-danger" role="alert">
                {{ session('errorMessage') }}
            </div>
        @endif
<form class="form-horizontal row-fluid" action="{{route('aspect.update', $aspectLigne->id)}}" method="POST">
    @csrf
    @method('PUT')
    <div class="control-group">
        <label class="control-label" for="basicinput">Aspect</label>
        <div class="controls">
            <input type="text" name="libelle" id="basicinput" value="{{$aspectLigne->Libelle}}" class="form-control @error('libelle') is-invalid @enderror"  class="span8" style="width: 50%">
            <span class="help-inline">Maximum 20 caractères</span>
        </div>
        @error('libelle')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Code</label>
        <div class="controls">
            <input type="text" name="code" id="basicinput" value="{{$aspectLigne->CodeAspect}}" class="form-control @error('code') is-invalid @enderror"  class="span8" style="width: 50%">
            <span class="help-inline">Numérique</span>
        </div>
        @error('code')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn" style="background: greenyellow">Valider</button>
        </div>
    </div>
</form>
@endsection
