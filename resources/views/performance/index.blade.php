@extends('layouts.dashboard')

@section('content')
<div class="module">
    <div class="module-head">
        <h3>
            Liste des personnes du club</h3>
    </div>
    <div class="module-body table">
        <table cellpadding="0" cellspacing="0" border="0"
            class="datatable-1 table table-bordered table-striped	 display" width="100%">
            <thead>
                <tr>
                    <th style="display: none">
                       <b>id</b>
                    </th>
                    <th>
                       <b>Clubs</b>
                    </th>
                    <th>
                       <b>Aspects</b>
                    </th>
                    <th>
                       <b>Années</b>
                    </th>
                    <th>

                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($listPerformance as $item)
                <tr class="odd gradeX">
                    <td style="display: none">
                        {{ $item->PerformanceId }}
                    </td>
                    <td>
                        {{ $item->NomClub }}
                    </td>
                    <td>
                        @if ($item->CodePerformance == 0)
                        {{ $item->Libelle }} <span style="float:right; background-color: green" class="badge success pos-rlt mr-2"><b class="arrow top b-success pull-in"></b>Bonne</span>
                        @else
                        {{ $item->Libelle }} <span style="float:right; background-color: red" class="badge success pos-rlt mr-2"><b class="arrow top b-success pull-in"></b>Mauvaise</span>
                        @endif
                    </td>
                    <td class="center">
                        [{{ $item->Date1 }} - {{ $item->Date2 }}]
                    </td>
                    <td style="text-align:right">
                        <a id="btnEdit"  title="Modifier" href="{{ route('performance.edit', $item->PerformanceId) }}"><i
                                class="fa fa-pencil text-default"></i></a>&nbsp; &nbsp; &nbsp;

                        <a id="btnDelete" title="Supprimer"
                            href="{{route('person.desable', ['id' =>$item->id]) }}"><i class="fa fa-power-off" style="color: rgb(220,20,60)"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


@endsection
