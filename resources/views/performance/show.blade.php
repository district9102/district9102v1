@extends('layouts.dashboard')

@section('content')
@if (session()->has('successMessage'))
            <div class="alert alert-success" role="alert">
                {{ session('successMessage') }}
            </div>
        @endif
        @if (session()->has('errorMessage'))
            <div class="alert alert-danger" role="alert">
                {{ session('errorMessage') }}
            </div>
        @endif
<form class="form-horizontal row-fluid" action="{{route('performance.update', $listPerformance->PerformanceId)}}" method="POST">
    @csrf
    @method('PUT')
    <div class="control-group">
        <label class="control-label" for="basicinput">Club : </label>
        <div class="controls">
            <div class="dropdown">
                <select id="nomClub" name="nomClub" style="width: 50%"
                    class="form-control select2  nomClub @error('nomClub') is-invalid @enderror" data-plugin="select2"
                    style="width: 25%">
                    <option disabled value="">Veuillez selectionner</option>
                    @foreach ($listClub as $item)
                    <option selected="{{$listPerformance->ClubId == $item->id}}"  value="{{ $item->id }}">
                         {{$item->NomClub}}
                    </option>
                    @endforeach
                </select>
                @error('nomClub') <span class="text-danger" role="alert"> {{ $message }}</span>
                @enderror

            </div>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Aspects : </label>
        <div class="controls">
            <div class="dropdown">
                <select id="libelle" name="libelle" style="width: 50%"
                    class="form-control select2  libelle @error('libelle') is-invalid @enderror" data-plugin="select2"
                    style="width: 25%">
                    <option disabled value="">Veuillez selectionner</option>
                    @foreach ($listAspect as $item)
                    <option selected="{{$listPerformance->AspectId == $item->id}}" value="{{ $item->id }}">
                         {{$item->Libelle}}
                    </option>
                    @endforeach
                </select>
                @error('libelle') <span class="text-danger" role="alert"> {{ $message }}</span>
                @enderror

            </div>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Etat : </label>
        <div class="controls">
            <div class="dropdown">
                <select id="etatPerformance" name="etatPerformance"
                    class="form-control select2  etatPerformance @error('etatPerformance') is-invalid @enderror" data-plugin="select2"
                    style="width: 50%">
                    <option disabled value="">Veuillez selectionner l'état</option>
                    <option value="0|Bonne">Bonne</option>
                    <option value="1|Mauvaise">Mauvaise</option>
                </select>
                @error('etatPerformance') <span class="text-danger" role="alert"> {{ $message }}</span>
                @enderror

            </div>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="basicinput">Année : </label>
        <div class="controls">
            <div class="dropdown">
                <select id="date" name="date" style="width: 50%"
                    class="form-control select2  date @error('date') is-invalid @enderror" data-plugin="select2"
                    style="width: 25%">
                    <option disabled value="">Veuillez selectionner</option>
                    @foreach ($listAnnee as $item)
                    <option selected="{{ $listPerformance->AnneeRotarienneId ==  $item->id }}" value="{{ $item->id }}">
                          [{{$item->Date1}} - {{$item->Date2}}]
                    </option>
                    @endforeach
                </select>
                @error('date') <span class="text-danger" role="alert"> {{ $message }}</span>
                @enderror

            </div>
        </div>
    </div>

    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn" style="background: greenyellow">Valider</button>
        </div>
    </div>
</form>
@endsection
