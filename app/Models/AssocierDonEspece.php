<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssocierDonEspece extends Model
{
    use HasFactory;
    protected $table = "AssocierDonEspece";

    public function personneassocierespece ()
    {
        return $this->belongsTo(User::class, 'PersonneId');
    }
}
