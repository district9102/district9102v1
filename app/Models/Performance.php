<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Performance extends Model
{
    use HasFactory;
    protected $table= 'Performance';

    protected $fillable = [
        'EtatPerformance',
        'CodePerformance'
    ];
    public function aspect ()
    {
        return $this->belongsTo(Aspect::class, 'AspectId');
    }

    public function personne ()
    {
        return $this->belongsTo(Personne::class, 'PersonneId');
    }

    public function anneerotarienne ()
    {
        return $this->belongsToMany(AnneeRotarienne::class,'Relier','PerformanceId', 'AnneeRotarienneId');
    }

    public function club ()
    {
        return $this->belongsToMany(Club::class,'Relier', 'PerformanceId', 'ClubId');
    }
}
