<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonEspece extends Model
{
    use HasFactory;
    protected $table = "DonEspece";

    protected $fillable = [
        'Libelle',
        'CodeDonEspece',
    ];

    public function evenementdonespece ()
    {
        return $this->belongsToMany(Evenement::class, 'AssocierDonEspece', 'EvenementId', 'DonEspeceId');
    }
}
