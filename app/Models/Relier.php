<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Relier extends Model
{
    use HasFactory;
    protected $table= 'Relier';
}
