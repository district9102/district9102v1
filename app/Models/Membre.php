<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Membre extends Model
{
    use HasFactory;
    protected $table= 'Membre';

    protected $fillable = [
        'DateAdhesion',
    ];

    public function role ()
    {
        return $this->belongsTo(Role::class, 'RoleId');
    }

    public function evenementvisiteur ()
    {
        return $this->belongsToMany(Evenement::class, 'Visiteur', 'EvenementId', 'ClubId', 'PersonneId');
    }
}
