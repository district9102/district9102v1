<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aspect extends Model
{
    use HasFactory;
    protected $table= 'Aspect';

    protected $fillable = [
        'Libelle',
        'CodeAspect'
    ];
}
