<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssocierDonNature extends Model
{
    use HasFactory;
    protected $table = "AssocierDonNature";

    protected $fillable = [
        'Nombre',
        'Montant',
    ];

    public function personneassociernature ()
    {
        return $this->belongsTo(User::class, 'PersonneId');
    }
}
