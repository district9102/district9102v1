<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeFinancement extends Model
{
    use HasFactory;
    protected $table = "TypeFinancement";

    protected $fillable = [
        'Libelle',
        'CodeTypeFinancement',
    ];

    public function evenement ()
    {
        return $this->belongsToMany(Evenement::class, 'Financement', 'EvenementId', 'TypeFinancementId');
    }
}
