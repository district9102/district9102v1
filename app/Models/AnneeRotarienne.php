<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnneeRotarienne extends Model
{
    use HasFactory;
    protected $table= 'AnneeRotarienne';

    protected $fillable = [
        'Date1',
        'Date2'
    ];

    public function performance ()
    {
        return $this->belongsToMany(Performance::class,'Relier', 'AnneeRotarienneId', 'PerformanceId');
    }
}
