<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeFrais extends Model
{
    use HasFactory;
    protected $table= 'TypeFrais';

    protected $fillable = [
        'Libelle',
        'CodeFrais'
    ];
}
