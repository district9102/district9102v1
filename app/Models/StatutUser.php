<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatutUser extends Model
{
    use HasFactory;
    protected $table= 'StatutUser';

    protected $fillable = [
        'Libelle',
        'CodeStatutUser' 
    ];
}
