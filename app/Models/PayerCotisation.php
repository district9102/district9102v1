<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PayerCotisation extends Model
{
    use HasFactory;
    protected $table= 'PayerCotisation';

    protected $fillable = [
        'Date',
    ];

    public function anneerotarienne ()
    {
        return $this->belongsTo(AnneeRotarienne::class, 'AnneeRotarienneId');
    }
}
