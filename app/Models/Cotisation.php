<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cotisation extends Model
{
    use HasFactory;
    protected $table= 'Cotisation';

    protected $fillable = [
        'Libelle',
        'Montant'
    ];

    public function personnes ()
    {
        return $this->belongsToMany(Personne::class, 'PersonneId');
    }
}
