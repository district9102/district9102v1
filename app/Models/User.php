<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table= 'Personne';

    protected $fillable = [
        'Nom',
        'Prenom',
        'DateNaissance',
        'Sexe',
        'email',
        'password',

    ];

    public function cotisations ()
    {
        return $this->belongsToMany(Cotisation::class, 'CotisationId');
    }

    public function typeuser ()
    {
        return $this->belongsTo(TypeUser::class, 'TypeUserId');
    }

    public function statutuser ()
    {
        return $this->belongsTo(StatutUser::class, 'StatutUserId');
    }

    public function club ()
    {
        return $this->belongsToMany(Club::class, 'Membre','PersonneId', 'ClubId');
    }

    public function evenementparticiper ()
    {
        return $this->belongsToMany(Evenement::class, 'Participer', 'EvenementId', 'PersonneId');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
