<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Frais extends Model
{
    use HasFactory;
    protected $table= 'Frais';

    protected $fillable = [
        'Montant',
    ];

    public function typefrais ()
    {
        return $this->belongsToMany(TypeFrais::class, 'TypeFraisId');
    }

    public function club ()
    {
        return $this->belongsTo(Club::class, 'ClubId');
    }
}
