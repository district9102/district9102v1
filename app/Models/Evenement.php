<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Evenement extends Model
{
    use HasFactory;
    protected $table= 'Evenement';

    protected $fillable = [
        'Titre',
        'DatePublication',
        'PieceJointe',
        'Jour',
        'HeureDebut',
        'HeureFin',
        'GrandPoint',
        'SynthesePoint1',
        'SynthesePoint2',
        'SynthesePoint3',
        'SynthesePoint4',
        'SynthesePoint5',
        'CommunicationStatutaire',
        'Quorum',
        'DescriptionCourte',
        'AxeStrategique',
        'RapportCommission1',
        'RapportCommission2',
        'RapportCommission3',
        'RapportCommission4',
        'RapportCommission5',
        'RapportCommission6',
        'RapportCommission7',
        'RapportCommission8',
        'RapportCommission9',
        'RapportCommission10',
        'LieuReunion',
        'VilleEvenement',
        'Preside',

    ];

    public function club ()
    {
        return $this->belongsTo(Club::class, 'ClubId');
    }

    public function typeevenement ()
    {
        return $this->belongsTo(TypeEvenement::class, 'TypeEvenementId');
    }

    public function typefinancement ()
    {
        return $this->belongsToMany(TypeFinancement::class, 'Financement', 'TypeFinancementId', 'EvenementId');
    }

    public function clubconjoint ()
    {
        return $this->belongsToMany(Club::class, 'ClubConjoint', 'ClubId', 'EvenementId');
    }

    public function membrevisiteur ()
    {
        return $this->belongsToMany(Membre::class, 'Visiteur', 'ClubId', 'PersonneId', 'EvenementId');
    }

    public function partenairesponsor ()
    {
        return $this->belongsToMany(Partenaire::class, 'Sponsor', 'PartenaireId', 'EvenementId');
    }

    public function personneparticiper()
    {
        return $this->belongsToMany(User::class, 'Participer', 'PersonneId', 'EvenementId');
    }

    public function donnature ()
    {
        return $this->belongsToMany(DonNature::class, 'AssocierDonNature', 'DonNatureId', 'EvenementId');
    }

    public function donespece ()
    {
        return $this->belongsToMany(DonEspece::class, 'AssocierDonEspece', 'DonEspeceId', 'EvenementId');
    }
}
