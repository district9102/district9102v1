<?php

namespace App\Models;

use App\Models\CategorieAction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Club extends Model
{
    use HasFactory;
    protected $table= 'Club';

    protected $fillable = [
        //'IdResponsable',
        'NomClub',
        'Historique',
        'ActionFM',

    ];

    public function personneresponsable ()
    {
        return $this->belongsTo(User::class, 'ResponsableId');
    }

    public function personne ()
    {
        return $this->belongsToMany(User::class,  'Membre','ClubId', 'PersonneId');
    }

    public function frais ()
    {
        return $this->belongsToMany(Frais::class, 'FraisId');
    }

    public function pays ()
    {
        return $this->belongsTo(Pays::class, 'PaysId');
    }

    public function performance ()
    {
        return $this->belongsToMany(Performance::class,'Relier','ClubId', 'PerformanceId');
    }

    public function categorie ()
    {
        return $this->belongsTo(CategorieAction::class, 'CategorieId');
    }

    public function evenementconjoint ()
    {
        return $this->belongsToMany(Evenement::class, 'ClubConjoint', 'EvenementId', 'ClubId');
    }
}
