<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Partenaire extends Model
{
    use HasFactory;
    protected $table = "Partenaire";

    protected $fillable = [
        'Nom',
        'Email',
        'Telephone',
        'Adresse',
        'Service',
        'CodePartenaire',
    ];

    public function evenementsponsor ()
    {
        return $this->belongsToMany(Evenement::class, 'Sponsor', 'EvenementId', 'PartenaireId');
    }
}
