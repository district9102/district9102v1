<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonNature extends Model
{
    use HasFactory;
    protected $table = "DonNature";

    protected $fillable = [
        'Libelle',
        'CodeDonNature',
    ];

    public function evenementnature ()
    {
        return $this->belongsToMany(Evenement::class, 'AssocierDonNature', 'EvenementId', 'DonNatureId');
    }
}
