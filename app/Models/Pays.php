<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pays extends Model
{
    use HasFactory;
    protected $table= 'Pays';

    protected $fillable = [
        'Intitule',
        'CodePays'
    ];
}
