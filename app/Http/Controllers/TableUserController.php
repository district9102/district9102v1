<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\StatutUser;

class TableUserController extends Controller
{
    //
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Demande  $demande
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
     public function indexTableUser()
    {
        /* $ListeUser=User::select('id', 'Nom', 'Prenom', 'email', 'DateNaissance', 'Sexe')
        ->get(); */
        $ListeUser=User::with('statutuser')->get();
        return view('user.tableUser', [
            'ListeUser'=>$ListeUser,
            ]);
    }
    public function supprimerUser(Request $request,$id)
    {
        $idUser = User::findOrFail($id);
        $gardIdStatutUser = StatutUser::select('id')
        ->where('CodeStatutUser', '=', 1)
        ->get();
        $affected=User::where('id', $idUser->id)
              ->update(['StatutUserId' => $gardIdStatutUser[0]->id]);
        /* $data =  User::where('id', $idDemande->id);
        $data->delete(); */
        return redirect()->route('indextableUser');

    }

    public function supprimerUserAll(Request $request)
    {
        //dd('laaa');
         $IdListe = explode(" ", $request->input('idD'));

         $gardIdStatutUser = StatutUser::select('id')
        ->where('CodeStatutUser', '=', 1)
        ->get();
          $affected=User::whereIn('id', $IdListe)
              ->update(['StatutUserId' => $gardIdStatutUser[0]->id]);
              return redirect()->route('indextableUser');

    }

    public function supprimerUserAllCocher(Request $request)
    {
        //dd('laaa');
         $IdListe = explode(" ", $request->input('idD2'));

         $gardIdStatutUser = StatutUser::select('id')
        ->where('CodeStatutUser', '=', 1)
        ->get();
          $affected=User::whereIn('id', $IdListe)
              ->update(['StatutUserId' => $gardIdStatutUser[0]->id]);
              return redirect()->route('indextableUser');

    }

    public function compteRenduReunion(Request $request)
    {
        return redirect()->route('evenementPublication');
    }

    public function retourAjouter(Request $request)
    {

              return redirect()->route('ajoutMembreVue');

    }

}
