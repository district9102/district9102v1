<?php

namespace App\Http\Controllers;

use App\Models\AssocierDonNature;
use Illuminate\Http\Request;

class AssocierDonNatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AssocierDonNature  $associerDonNature
     * @return \Illuminate\Http\Response
     */
    public function show(AssocierDonNature $associerDonNature)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AssocierDonNature  $associerDonNature
     * @return \Illuminate\Http\Response
     */
    public function edit(AssocierDonNature $associerDonNature)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AssocierDonNature  $associerDonNature
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssocierDonNature $associerDonNature)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AssocierDonNature  $associerDonNature
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssocierDonNature $associerDonNature)
    {
        //
    }
}
