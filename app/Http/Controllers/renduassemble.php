<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class renduassemble extends Controller
{
    public function create ()
    {
        $date = date('Y-m-d');
        return view('compterenduassemble.create', [
            'date'=>$date,
            'listPreside'=>User::all(),
        ]);
    }

    public function store (Request $request)
    {
        $request->validate([
            'preside'=>'required',
            'heureDebut1'=>'required|numeric',
            'heureDebut2'=>'required|numeric',
            'heureFin1'=>'required|numeric',
            'heureFin2'=>'required|numeric'
        ]);
    }
}
