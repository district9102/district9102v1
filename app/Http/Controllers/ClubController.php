<?php

namespace App\Http\Controllers;

use App\Models\Club;
use App\Models\Pays;
use App\Models\Role;
use App\Models\User;
use App\Models\Aspect;
use App\Models\CategorieAction;
use App\Models\Membre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Symfony\Component\Console\Input\Input;

class ClubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /* $user = DB::table('Membre')
                ->join('Personne', 'Personne.id', '=', 'Membre.PersonneId')
                ->join('Club', 'Club.id', '=', 'Membre.ClubId')
                ->select('Membre.*', 'Personne.*', 'Club.*')
                ->get();
        dd($user); */

        $club = Club::with('personneresponsable')->get();
        //dd($club);
        return view('club.index', [
            'listClub' =>$club
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('club.create', [
            'personneList' => User::all(),
            'paysList' => Pays::all(),
            'categorieList'=>CategorieAction::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd('dddd');
        $request->validate([
            'nomclub' => 'required|max:20|unique:Club,NomClub,',
            'nomResponsable' => 'required',
            'actionfm' => 'required|max:50',
            'historique' => 'required|max:50',
            'nomPays' => 'required',
            'nomCategorie'=>'required'
        ]);
        //dd((int)$request->nomResponsable);

        try {
            $club = Club::make([
                //'PersonneId' => (int)$request->nomResponsable,
                'NomClub' => $request->nomclub,
                'Historique' => $request->historique,
                'ActionFM' => $request->actionfm,
                //'PaysId' => (int)$request->nomPays
            ]);
            $club->pays()->associate((int)$request->nomPays);
            $club->personneresponsable()->associate((int)$request->nomResponsable);
            $club->categorie()->associate($request->nomCategorie);
            $club->save();
        } catch (QueryException $ex) {
            return back()->with('errorMessage', "Echec de la création du club [".$request->nomclub."]");
        }

        $role = Role::where('CodeRole', 0)->first();

        $membre = new Membre();
        $membre ->ClubId = $club->id;
        $membre ->PersonneId = (int)$request->nomResponsable;
        $membre->DateAdhesion = date('Y-m-d');
        $membre->RoleId =$role->id;
        $membre->save();
        return back()->with('successMessage', "Le Club [".$request->nomclub."] a été crée avec succès");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Club  $club
     * @return \Illuminate\Http\Response
     */
    public function show(Club $club)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Club  $club
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $club = Club::find($id);

        $clubpersonne = Club::where('id', $id)->with('personneresponsable', 'pays')->first();
        //$clubpersonne = User::where('id', $id)->with('club')->first();
        //dd($clubpersonne);

        return view('club.show', [
            'listClub'=>$clubpersonne,
            'listPersonne'=>User::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Club  $club
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nomclub'=>'required|max:20',
            'nomResponsable'=>'required',
            'actionfm'=>'required|max:20'
        ]);
        try {
        $currentData = Club::find($id);
        $currentData->NomClub = $request->nomclub;
        $currentData->personneresponsable()->associate((int)$request->nomResponsable);
        $currentData->ActionFM = $request->actionfm;
        $currentData->save();
        return back()->with('successMessage', "Le club [" . $request->nomclub. "] ont été modifié avec succès");
        } catch (QueryException $ex) {
            return back()->with('errorMessage', "Echec de la modification du club [" . $request->nomclub. "]");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Club  $club
     * @return \Illuminate\Http\Response
     */
    public function destroy(Club $club)
    {
        //
    }


}
