<?php

namespace App\Http\Controllers;

use App\Models\Club;
use App\Models\User;
use App\Models\Aspect;
use App\Models\Relier;
use App\Models\TypeUser;
use App\Models\Performance;
use Illuminate\Http\Request;
use App\Models\AnneeRotarienne;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class PerformanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $performance = DB::table('Relier')
            ->join('Club', 'Club.id', '=', 'Relier.ClubId')
            ->join('Performance', 'Performance.id', '=', 'Relier.PerformanceId')
            ->join('AnneeRotarienne', 'AnneeRotarienne.id', '=', 'Relier.AnneeRotarienneId')
            ->join('Aspect', 'Aspect.id', '=', 'Performance.AspectId')
            ->select('Club.*', 'Performance.*', 'Aspect.*', 'AnneeRotarienne.*', 'Relier.*')
            ->get();
        //dd($performance);



        //$performance = Performance::with('anneerotarienne', 'club', 'aspect')->get();
        //dd($performance);
        return view('performance.index', [
            'listPerformance' => $performance
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('performance.create', [
            'listClub' => Club::all(),
            'listAspect' => Aspect::all(),
            'listAnnee' => AnneeRotarienne::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nomClub' => 'required',
            'libelle' => 'required',
            'etatPerformance' => 'required',
            'date' => 'required'
        ]);

        $etat = explode('|', $request->etatPerformance);
        $type = TypeUser::where('CodeTypeUser', 1)->first();
        //dd($type->id);
        $personne = User::where('TypeUserId', $type->id)->first();
        //dd($personne->id);

        $performance = DB::table('Relier')
            ->join('Club', 'Club.id', '=', 'Relier.ClubId')
            ->join('Performance', 'Performance.id', '=', 'Relier.PerformanceId')
            ->join('AnneeRotarienne', 'AnneeRotarienne.id', '=', 'Relier.AnneeRotarienneId')
            ->join('Aspect', 'Aspect.id', '=', 'Performance.AspectId')
            ->select('Club.*', 'Performance.*', 'Aspect.*', 'AnneeRotarienne.*')
            ->where('Club.id', $request->nomClub)
            ->where('Aspect.id', $request->libelle)
            ->where('Performance.CodePerformance', (int)$etat[0])
            ->where('AnneeRotarienne.id', $request->date)
            ->first();

        /*  $verify = $performance->where('NomClub', $request->nomClub)
            ->where('Libelle', $request->libelle)
            ->where('CodePerformance', (int)$etat[0])
            ->where('id', $request->date)
            ->first(); */
        //dd($verify);
        //dd($performance);



        if ($performance) {
            return back()->with('errorMessage', "Performance déjà attribuée");
        } else {
            try {
                $performance = Performance::make([
                    'EtatPerformance' => $etat[1],
                    'CodePerformance' => (int)$etat[0],
                ]);
                $performance->PersonneId = $personne->id;
                $performance->aspect()->associate($request->libelle);
                $performance->save();
            } catch (QueryException $ex) {
                return back()->with('errorMessage', "Performance du club non attribuée");
            }

            $relier = new Relier();
            $relier->AnneeRotarienneId = $request->date;
            $relier->PerformanceId = $performance->id;
            $relier->ClubId = $request->nomClub;
            //dd('ddd');
            $relier->save();

            return back()->with('successMessage', "La performance du club a été affectée avec succès");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Performance  $performance
     * @return \Illuminate\Http\Response
     */
    public function show(Performance $performance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Performance  $performance
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $performance = DB::table('Relier')
            ->join('Club', 'Club.id', '=', 'Relier.ClubId')
            ->join('Performance', 'Performance.id', '=', 'Relier.PerformanceId')
            ->join('AnneeRotarienne', 'AnneeRotarienne.id', '=', 'Relier.AnneeRotarienneId')
            ->join('Aspect', 'Aspect.id', '=', 'Performance.AspectId')
            ->select('Club.*', 'Performance.*', 'Aspect.*', 'AnneeRotarienne.*', 'Relier.*')
            ->where('Performance.id', $id)
            ->first();



        // dd($performance);
        /* $performance = Performance::find($id);

        $aspect = Aspect::where('id', $performance->AspectId)->first();

        $relier = Relier::where('PerformanceId', $id)->first();

        $club = Club::where('id', $relier->ClubId)->first();

        $annee = AnneeRotarienne::where('id', $relier->AnneeRotarienneId)->first(); */



        return view('performance.show', [
            'listPerformance' => $performance,
            'listClub' => Club::all(),
            'listAspect' => Aspect::all(),
            'listAnnee' => AnneeRotarienne::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Performance  $performance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($id);
        $performance = DB::table('Relier')
            ->join('Club', 'Club.id', '=', 'Relier.ClubId')
            ->join('Performance', 'Performance.id', '=', 'Relier.PerformanceId')
            ->join('AnneeRotarienne', 'AnneeRotarienne.id', '=', 'Relier.AnneeRotarienneId')
            ->join('Aspect', 'Aspect.id', '=', 'Performance.AspectId')
            ->select('Club.*', 'Performance.*', 'Aspect.*', 'AnneeRotarienne.*', 'Relier.*')
            ->where('Performance.id', $id)
            ->first();

        //dd($performance);

        $request->validate([
            'nomClub' => 'required',
            'libelle' => 'required',
            'etatPerformance' => 'required',
            'date' => 'required'
        ]);

        $etat = explode('|', $request->etatPerformance);
        $type = TypeUser::where('CodeTypeUser', 1)->get();
        $personne = User::where('TypeUserId', $type[0]->id)->first();


        try {
            $currentData = Performance::find($id);
            $currentData->EtatPerformance = $etat[1];
            $currentData->CodePerformance = $etat[0];
            $currentData->PersonneId = $personne->id;
            $currentData->aspect()->associate($request->libelle);
            $currentData->save();
        } catch (QueryException $ex) {
            return back()->with('errorMessage', "Performance non modifiée");
        }

        //$relier = Relier::find($id);
        $relier = Relier::where('PerformanceId', $id)->update([
            'AnneeRotarienneId'=>$request->date,
            'ClubId'=>$request->nomClub,

        ]);
        //dd($relier);
        /* $relier->AnneeRotarienneId = $request->date;
        $relier->ClubId = $request->nomClub;
        $relier->save(); */
        return back()->with('successMessage', "La performance a été modifiée");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Performance  $performance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Performance $performance)
    {
        //
    }
}
