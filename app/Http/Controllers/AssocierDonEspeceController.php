<?php

namespace App\Http\Controllers;

use App\Models\AssocierDonEspece;
use Illuminate\Http\Request;

class AssocierDonEspeceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AssocierDonEspece  $associerDonEspece
     * @return \Illuminate\Http\Response
     */
    public function show(AssocierDonEspece $associerDonEspece)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AssocierDonEspece  $associerDonEspece
     * @return \Illuminate\Http\Response
     */
    public function edit(AssocierDonEspece $associerDonEspece)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AssocierDonEspece  $associerDonEspece
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssocierDonEspece $associerDonEspece)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AssocierDonEspece  $associerDonEspece
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssocierDonEspece $associerDonEspece)
    {
        //
    }
}
