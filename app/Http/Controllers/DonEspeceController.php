<?php

namespace App\Http\Controllers;

use App\Models\DonEspece;
use Illuminate\Http\Request;

class DonEspeceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DonEspece  $donEspece
     * @return \Illuminate\Http\Response
     */
    public function show(DonEspece $donEspece)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DonEspece  $donEspece
     * @return \Illuminate\Http\Response
     */
    public function edit(DonEspece $donEspece)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DonEspece  $donEspece
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DonEspece $donEspece)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DonEspece  $donEspece
     * @return \Illuminate\Http\Response
     */
    public function destroy(DonEspece $donEspece)
    {
        //
    }
}
