<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Club;
use App\Models\TypeFrais;
use App\Models\Frais;
use App\Models\Payer;
use App\Models\AnneeRotarienne;
use Pay_Setup;
use Pay_Checkout_Store;
use Pay_Checkout_Invoice;

class FraisController extends Controller
{
    //

    public function fraisParticipation()
    {
        $date = date('Y-m-d');
        return view('user.frais', compact('date'), [
            'ClubList' => Club::all(),
            'TypeFrais' => TypeFrais::all(),
            'AnneeRotarienneListe' => AnneeRotarienne::all(),
        ]);
    }

    public function ValidationFraisParticipation(Request $request)
    {

        $request->validate([
            'TypeFrais' => ['required'],
            'AnneeRotarienneListe' => ['required'],
            'Montant' => ['required', 'numeric'],
        ]);
        require_once(app_path() . '/pay-php-gateway/pay-php-gateway.php');
        $setup = new Pay_Setup();
        $setup->setApi_key("EZNC2BL7WA257VL8Z");
        $setup->setMode("test");
        $setup->setToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZF9hcHAiOiI0MiIsImlkX2Fib25uZSI6IjY0IiwiZGF0ZWNyZWF0aW9uX2FwcCI6IjIwMjEtMDEtMTggMTU6NDQ6NTgifQ.mkxNrQ89Kf0ogHmHT_Y9SVMeQvlI-Vd06GB0cWQsRsw");

        $store = new Pay_Checkout_Store();
        $store->setName("district9102v1");
        $store->setWebsiteUrl("https://127.0.0.1:8000");
        $store->setCancelUrl("");
        $store->setCallbackUrl("https://127.0.0.1:8000");
        $store->setReturnUrl("");

        $co = new Pay_Checkout_Invoice($store, $setup);
        # Ajoute des articles
        //dd($request->Montant);
        $co->addItem("Frais de participation", 1, $request->Montant, $request->Montant, "Jean bleu, de marque Gucci");
        $co->setTotalAmount($request->Montant);
        $co->setDescription("Frais de participation");

        if ($co->create()) {
            # Requête acceptée, alors on redirige le client vers la page de validation de paiement
            // header("Location: ".$co->getInvoiceUrl());
            return redirect()->to($co->getInvoiceUrl());

            // dd();
        } else {
            #Requête refusée, alors on affiche le motif du rejet
            dd($co->response_text);
        }

        if ($co->confirm()) {
            # Transaction réussie
            $frais = Frais::make([
                'Montant' => $request->Montant,
            ]);
            $frais->typefrais()->associate($request->input('TypeFrais'));
            $frais->save();

            $gardIdFrais = Frais::select('id')
                ->latest()
                ->first()
                ->get();

            $date = explode("/", $request->input('AnneeRotarienneListe'));

            $gardIdAnneeRotarienne = AnneeRotarienne::select('id')
                ->Where([
                    ['Date1', '=', $date[0]],
                    ['Date2', '=', $date[1]],
                ])
                ->get();

                $date = date('Y-m-d');
            $gardIdClub = $request->session()->get('idclub');
            $payerFrais = new Payer();
            $payerFrais->ClubId = $gardIdClub;
            $payerFrais->FraisId = $gardIdFrais[0]->id;
            $payerFrais->DatePayer = $date;
            return redirect()->route('paiement');
        } else {
            # Transaction échouée
            return redirect()->back();
        }
    }
}
