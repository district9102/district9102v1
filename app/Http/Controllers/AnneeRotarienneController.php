<?php

namespace App\Http\Controllers;

use App\Models\AnneeRotarienne;
use App\Models\Relier;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class AnneeRotarienneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('anneerotarienne.index', [
            'listAnnee'=> AnneeRotarienne::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('anneerotarienne.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd('ff');
        $request->validate([
            'date1' => 'required|date',
            'date2' => 'required|date',
        ]);
        //dd($request->date1);

        if ($request->date1>=$request->date2) {
            return back()->with('errorMessage', "Période non valide");
        } else {
            try {
                AnneeRotarienne::create([
                    'Date1'=>$request->date1,
                    'Date2'=>$request->date2
                ]);
                return back()->with('successMessage', "Date crée");
            } catch (QueryException $ex) {
                return back()->with('errorMessage', "Date non crée");
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AnneeRotarienne  $anneeRotarienne
     * @return \Illuminate\Http\Response
     */
    public function show(AnneeRotarienne $anneeRotarienne)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AnneeRotarienne  $anneeRotarienne
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $annee = AnneeRotarienne::find($id);
        return view('anneerotarienne.show', [
            'listAnnee'=>$annee
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AnneeRotarienne  $anneeRotarienne
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'date1'=>'required|date',
            'date2'=>'required|date'
        ]);

        try {
            $currentData = AnneeRotarienne::find($id);
            $currentData ->Date1 = $request->date1;
            $currentData->Date2 = $request->date2;
            $currentData->save();
            return back()->with('successMessage', "Date modifiée");

        } catch (QueryException $ex) {
            return back()->with('errorMessage', "Erreur, date non modifiée");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AnneeRotarienne  $anneeRotarienne
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        dd('ffff');
        $annee = Relier::find($id);
        if ($annee) {
            return back()->with('errorMessage', "Impossible de supprimer cette date, Liaison avec d'autres tables");
        } else {
            AnneeRotarienne::destroy($id);
            return back()->with('successMessage', "La date a été supprimer avec succès");
        }

    }
}
