<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\StatutUser;
use App\Models\TypeUser;
use App\Models\Membre;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;

class AjoutMembreController extends Controller
{

    public function indexAjoutMembre()
    {
        $date=date('Y-m-d');
        return view('user.ajoutMembre',compact('date'), [
            'RoleListe'=>Role::select('id','LibelleRole')
            ->whereIn('CodeRole', [1, 2])
                    ->get(),
        ]);
    }

       public function ajoutMembre(Request $request)
    {

        $date=date('Y-m-d');
         //dd(date('y-m-d '));
        // dd($request->input('DateNaissance'));
        $request->validate([
            'Nom' => ['required','string','max:255'],
            'Prenom' => ['required','string','max:255'],
            'email' => ['required','email','max:255', 'unique:Personne'],
            'Sexe' => ['required'],
            'Role' => ['required'],
            'DateNaissance' => ['required','date','before_or_equal:today'],
            'password' => ['required','string','min:8'],
            'confirmpassword' => ['required','string','min:8','same:password'],
        ]);
        //$infouser = Auth::user();
        //dd($infouser);
        //dd($request->input('pets'));
        /* if (($validator->fails())) {

        $errors  =  $validator->errors();
            return view('user.ajoutMembre',compact('errors'));
        }  */
        //dd($request->input('Role'));


        $gardIdStatutUser = StatutUser::select('id')
        ->where('CodeStatutUser', '=', 0)
        ->get();
        $gardIdTypeUser = TypeUser::select('id')
        ->where('CodeTypeUser', '=', 0)
        ->get();
       // dd($gardIdStatutUser[0]->id);

                $user=User::make([
                    'Nom' => $request->input('Nom'),
                    'Prenom' => $request->input('Prenom'),
                    'DateNaissance' => $request->input('DateNaissance'),
                    'Sexe' => $request->input('Sexe'),
                    'email' => $request->input('email'),
                    'password' => bcrypt($request->input('password')),
                ]);
                $user->statutuser()->associate($gardIdStatutUser[0]->id);
                $user->typeuser()->associate($gardIdTypeUser[0]->id);
                $user->save();

                /* $gardIdUser = User::select('id')
                ->latest()
                ->first()
                ->get(); */


                ///dd($user);
/* dd($gardIdUser[0]->id);
        $membre=Membre::create([
            'PersonneId' => $gardIdUser[0]->id,
            'ClubId' => 1,
            'RoleId' =>$request->input('Role'),
            'DateAdhesion'=>$date,
        ]); */

        $gardIdClub=$request->session()->get('idclub');
        $membre=new Membre();
        $membre->PersonneId=$user->id;
        $membre->ClubId=$gardIdClub;
        $membre->RoleId=(int)$request->input('Role');
        $membre->DateAdhesion=$date;
        //dd($membre);
        $membre->save();

                return redirect()->route('indextableUser');


}
}
