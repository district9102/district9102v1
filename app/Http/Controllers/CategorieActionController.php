<?php

namespace App\Http\Controllers;

use App\Models\CategorieAction;
use Illuminate\Http\Request;

class CategorieActionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CategorieAction  $categorieAction
     * @return \Illuminate\Http\Response
     */
    public function show(CategorieAction $categorieAction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CategorieAction  $categorieAction
     * @return \Illuminate\Http\Response
     */
    public function edit(CategorieAction $categorieAction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CategorieAction  $categorieAction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CategorieAction $categorieAction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CategorieAction  $categorieAction
     * @return \Illuminate\Http\Response
     */
    public function destroy(CategorieAction $categorieAction)
    {
        //
    }
}
