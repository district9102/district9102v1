<?php

namespace App\Http\Controllers;

use App\Models\Aspect;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class AspectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('aspect.index', [
            'listAspect' => Aspect::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('aspect.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'libelle' => 'required',
            'code' => 'required|numeric'
        ]);

        $aspectlibelle = Aspect::where('Libelle', $request->libelle)->first();

        $aspectcode = Aspect::where('CodeAspect', $request->code)->first();


        if ($aspectlibelle) {
            return back()->with('errorMessage', "Cet aspect existe déjà");
        } else {
            if ($aspectcode) {
                return back()->with('errorMessage', "Le code de aspect existe déjà");
            } else {
                try {
                    Aspect::create([
                        'Libelle' => $request->libelle,
                        'CodeAspect' => $request->code
                    ]);
                    return back()->with('successMessage', "[" . $request->libelle . "] a été ajouté avec succès");
                } catch (QueryException $ex) {
                    return back()->with('errorMessage', "[" . $request->libelle . "] non ajoutée");
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Aspect  $aspect
     * @return \Illuminate\Http\Response
     */
    public function show(Aspect $aspect)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Aspect  $aspect
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $aspect = Aspect::findOrFail($id);
        return view('aspect.show', [
            'aspectLigne' => $aspect
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Aspect  $aspect
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'libelle' => "required|max:20|unique:Aspect,Libelle,$id",
            'code' => "required|numeric|unique:Aspect,CodeAspect,$id"
        ]);

        /* $aspectlibelle = Aspect::where('id', "<>", $id)
            ->where(function ($query) use ($request) {
                $query->where('Libelle', $request->libelle)
                    ->orWhere('CodeAspect', $request->code);
            })
            ->first(); */



            try {
                $currentData = Aspect::findOrFail($id);
                $currentData->Libelle = $request->libelle;
                $currentData->CodeAspect = $request->code;
                $currentData->save();
                return back()->with('successMessage', "Aspect modifié");
            } catch (QueryException $ex) {
                return back()->with('errorMessage', "Aspect non modifié");

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Aspect  $aspect
     * @return \Illuminate\Http\Response
     */
    public function destroy(Aspect $aspect)
    {
        //
    }
}
