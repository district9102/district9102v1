<?php

namespace App\Http\Controllers;

use App\Models\ClubConjoint;
use Illuminate\Http\Request;

class ClubConjointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ClubConjoint  $clubConjoint
     * @return \Illuminate\Http\Response
     */
    public function show(ClubConjoint $clubConjoint)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ClubConjoint  $clubConjoint
     * @return \Illuminate\Http\Response
     */
    public function edit(ClubConjoint $clubConjoint)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ClubConjoint  $clubConjoint
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClubConjoint $clubConjoint)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ClubConjoint  $clubConjoint
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClubConjoint $clubConjoint)
    {
        //
    }
}
