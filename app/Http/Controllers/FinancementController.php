<?php

namespace App\Http\Controllers;

use App\Models\Financement;
use Illuminate\Http\Request;

class FinancementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Financement  $financement
     * @return \Illuminate\Http\Response
     */
    public function show(Financement $financement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Financement  $financement
     * @return \Illuminate\Http\Response
     */
    public function edit(Financement $financement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Financement  $financement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Financement $financement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Financement  $financement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Financement $financement)
    {
        //
    }
}
