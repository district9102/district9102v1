<?php

namespace App\Http\Controllers;

use App\Models\Club;
use App\Models\User;
use App\Models\Membre;
use App\Models\Personne;
use App\Models\TypeUser;
use App\Models\StatutUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;

class PersonneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Liste = User::with('statutuser')->get();
        //$DemandesListe = $Liste[0]->statutuser->CodeStatutUser ;
        //dd($Liste);
        //dd($Liste[0]->statutuser);
        //dd($Liste[0]->statutuser->CodeStatutUser);
        return view('personne.index', [
            'listPersonne' => $Liste,
            //'couleur'=>$DemandesListe,
            'listType' => TypeUser::all(),
            'listStatut' => StatutUser::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $date = date('Y-m-d');
        return view('personne.create', compact('date'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nom' => 'required|max:20',
            'prenom' => 'required|max:20',
            'datenaissance' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8',
            'passwordconfirm' => 'required|min:8',
            'sexe' => 'required',
        ]);

        $typeuser = TypeUser::where('CodeTypeUser', 0)->first();

        $statutuser = StatutUser::where('CodeStatutUser', 0)->first();
        //dd($request->all());
        $date = date('Y-m-d');

        if ($request->datenaissance <= $date) {
            if ($request->password == $request->passwordconfirm) {
                try {
                    $user =   User::make([
                        'Nom' => $request->nom,
                        'Prenom' => $request->prenom,
                        'DateNaissance' => $request->datenaissance,
                        'email' => $request->email,
                        'password' => bcrypt($request->password),
                        'Sexe' => $request->sexe,

                    ]);
                    $user->typeuser()->associate($typeuser);
                    $user->statutuser()->associate($statutuser);
                    $user->save();
                    return back()->with('successMessage', "La personne [" . $request->nom .' '. $request->prenom . "] a été crée avec succès");
                } catch (QueryException $ex) {
                    return back()->with('errorMessage', "Echec de la création de [" . $request->nom .' '. $request->prenom . "]");
                }
            } else {
                return back()->with('errorMessage', "Echec de la création de [" . $request->nom .' '. $request->prenom . "]");
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Personne  $personne
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Personne  $personne
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $personne = User::find($id);
        if ($personne)
            return view('personne.show', [
                'listPersonne' => $personne,
                'listStatut' => StatutUser::all()
            ]);
        else
            return redirect(route('personne.index'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Personne  $personne
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nom' => 'required|max:20',
            'prenom' => 'required|max:20',
            'datenaissance' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8',
            'sexe' => 'required',
        ]);
        $typeuser = TypeUser::where('CodeTypeUser', 0)->first();
        //dd($typeuser->id);
        //dd($request->libelle);

        try {
            $currentData = User::find($id);
            $currentData->Nom = $request->nom;
            $currentData->Prenom = $request->prenom;
            $currentData->DateNaissance = $request->datenaissance;
            $currentData->Sexe = $request->sexe;
            $currentData->email = $request->email;
            $currentData->password = bcrypt($request->password);
            $currentStatut = StatutUser::find($request->libelle);
            $currentData->statutuser()->associate($currentStatut);
            $currentData->save();
            return back()->with('successMessage', "Les informations de [" . $request->nom . $request->prenom . "] ont été modifiée avec succès");
        } catch (QueryException $ex) {
            return back()->with('errorMessage', "Echec de la modification de [" . $request->nom . $request->prenom . "]");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Personne  $personne
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $statutuser = StatutUser::where('CodeStatutUser', 0)->first();

        try {
            $currentData = User::find($id);
            //$currentStatut = StatutUser::find($request->libelle);
            $currentData->statutuser()->associate($statutuser->id);
            $currentData->save();
            return back()->with('successMessage', "Le membre a été désactiver avec succès");
        } catch (QueryException $ex) {
            return back()->with('errorMessage', "Echec de la désactivation");
        }
    }

    public function disable($id)
    {
        //dd($id);
        $statutuser = StatutUser::where('CodeStatutUser', 1)->first();
        //dd($statutuser->id);

        try {
            $currentData = User::find($id);
            //$currentStatut = StatutUser::find($request->libelle);
            $currentData->statutuser()->associate($statutuser->id);
            $currentData->save();
            return back()->with('successMessage', "Le membre a été désactiver avec succès");
        } catch (QueryException $ex) {
            return back()->with('errorMessage', "Echec de la désactivation");
        }
    }

    public function verification(Request $request)
    {
        $request->validate([
            'email'=>'required|email',
            'password'=>'required',
            'nomClub'=>'required'
        ]);

        if( Auth::attempt(['email' => $request->email, 'password' => $request->password]))
        {
            $user = Auth::user();
            $club = Club::findorFail($request->nomClub);
            $membre = Membre::where('PersonneId', $user->id)
            ->where('ClubId', $club->id)->first();
            if (!is_null($membre) ) {
                $request->session()->put('idclub', $club->id);
                return redirect()->route('bienvenue');
            } else {
                Auth::logout();
                return back()->with('errorMessage', "Vos informations ne sont pas correctes");
            }

        }


    }
}
