<?php

namespace App\Http\Controllers;

use App\Models\Club;
use App\Models\Membre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class verification extends Controller
{
    public function verification(Request $request)
    {
        $request->validate([
            'email'=>'required|email',
            'password'=>'required',
            'nomClub'=>'required'
        ]);

        if( Auth::attempt(['email' => $request->email, 'password' => $request->password]))
        {
            //dd('fff');
            $user = Auth::user();
            $club = Club::findorFail($request->nomClub);
            $membre = Membre::where('PersonneId', $user->id)
            ->where('ClubId', $club->id)->first();
            if (!is_null($membre) ) {
                $request->session()->put('idclub', $club->id);
                return redirect()->route('bienvenue');
            } else {
                Auth::logout();
                return back()->with('errorMessage', "Vos informations ne sont pas correctes");
            }

        }


    }
}
