<?php

namespace App\Http\Controllers;
use Carbon\Carbon;

use App\Models\Evenement;
use App\Models\Club;
use Illuminate\Http\Request;

class EvenementController extends Controller
{
    public function evenement()
    {
        $date=date('Y-m-d');
        return view('user.evenementPublication',compact('date'), [
            'ClubList'=>Club::all(),
            ]);
    }

    public function evenementPublication(Request $request)
    {
        $date=date('Y-m-d');
        //dd(date("Y-m-d H:i"));
        $request->validate([
            'Titre' => ['required','string','max:255'],
           // 'DatePublication' => ['required','date','date_equals:today'],
            'JourHeure' => ['required'],
            'LieuReunion' => ['required','string','max:255'],
            'VilleEvenement' => ['required','string','max:255'],
            'PieceJointe' => ['required','file','mimes:jpeg,pdf,txt,odt,png'],
        ]);
        //dd($datetime[0])
        //dd($gardIdClub[0]->id);

        $path = request('PieceJointe')->store('Piece');
        $theDay = Carbon::make($request->input('JourHeure'));
        $theDay->isToday();
        $theDay->isPast();
        $theDay->isFuture();
        if($theDay->lt(Carbon::today()) || $theDay->eq(Carbon::today())) {
            $datetime = explode("T", $request->input('JourHeure'));
               $user=Evenement::make([
                    'Titre' => $request->input('Titre'),
                    'DatePublication' => $date,
                    'PieceJointe' => $path,
                    'Jour' => $datetime[0],
                    'Heure' => $datetime[1],
                    'LieuReunion' => $request->input('LieuReunion'),
                    'VilleEvenement' => $request->input('VilleEvenement'),

                ]);
              /*   $nomClub = $request->input('NomClub');
                //dd($nomClub);
                $gardIdClub = Club::select('id')
                ->where('NomClub', '=', $nomClub)
                ->get(); */
                $gardIdClub=$request->session()->get('idclub');

               // dd($gardIdClub[0]->id);
                $user->club()->associate($gardIdClub);
                $user->typeevenement()->associate(1);
                $user->save();
                return redirect()->route('indextableUser');
        }else{
            $iff=$request->all();
            session()->flash('error','La date d\'annonce doit etre antérieure ou égale à la date d\'aujourd\'hui');
            return view('user.evenementPublication',compact('date','iff'), [
            'ClubList'=>Club::all(),
            ]);
        }
       // dd($gardIdStatutUser[0]->id);
    }
}
