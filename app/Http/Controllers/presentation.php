<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class presentation extends Controller
{
    public function rotary ()
    {
        return view('presentation.rotaract');
    }

    public function interact ()
    {
        return view('presentation.interact');
    }

    public function district9102 ()
    {
        return view('presentation.district9102');
    }
    public function ryla ()
    {
        return view('presentation.ryla');
    }
    public function acd ()
    {
        return view('presentation.acd');
    }
}
