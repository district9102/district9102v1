<?php

namespace App\Http\Controllers;

use App\Models\StatutUser;
use Illuminate\Http\Request;

class StatutUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('statutuser.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StatutUser  $statutUser
     * @return \Illuminate\Http\Response
     */
    public function show(StatutUser $statutUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StatutUser  $statutUser
     * @return \Illuminate\Http\Response
     */
    public function edit(StatutUser $statutUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StatutUser  $statutUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StatutUser $statutUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StatutUser  $statutUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(StatutUser $statutUser)
    {
        //
    }
}
