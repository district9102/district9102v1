<?php

namespace App\Http\Controllers;

use App\Models\AnneeRotarienne;
use App\Models\Performance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Bienvenu extends Controller
{
    public function bienvenue ()
    {
        $annee = DB::table('AnneeRotarienne')
                ->latest()
                ->first();

        $club = session()->get('idclub');
        //dd($club);

        $performance = DB::table('Relier')
            ->join('Club', 'Club.id', '=', 'Relier.ClubId')
            ->join('Performance', 'Performance.id', '=', 'Relier.PerformanceId')
            ->join('AnneeRotarienne', 'AnneeRotarienne.id', '=', 'Relier.AnneeRotarienneId')
            ->join('Aspect', 'Aspect.id', '=', 'Performance.AspectId')
            ->select('Club.*', 'Performance.*', 'Aspect.*', 'AnneeRotarienne.*', 'Relier.*')
            ->where('AnneeRotarienne.id', $annee->id)
            ->where('Club.id', $club)
            ->get();
        //dd($performance);


        return view('welcomeadmin', [
            'listePerformance'=>$performance
        ]);
    }

    public function frais ()
    {
        return view('frais.create');
    }
}
